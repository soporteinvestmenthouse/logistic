<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shipment_Status extends Model
{
    use SoftDeletes;
	
	protected $table = 'shipment_statuses';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function color()
	{
		switch($this->type)
		{
			case "Negative":
				return "danger";
			case "Neutral":
				return "primary";
			case "Positive":
				return "success";
		}
	}
}
