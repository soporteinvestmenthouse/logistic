<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseShipment extends Model
{
    use SoftDeletes;
	
	protected $table = 'purchaseshipments';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $fillable = ['purchase_id', "shipment_id"];

	protected $dates = ['deleted_at'];

	public function shipment() 
	{
		return $this->belongsTo(Shipment::class);
	}
}
