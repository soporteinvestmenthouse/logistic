<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use App\ModelLogic\ShipmentUpdateLogic;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shipment_Update extends Model
{
    use SoftDeletes, ShipmentUpdateLogic;
	
	protected $table = 'shipment_updates';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function statusElement()
	{
		return $this->belongsTo(Shipment_Status::class, "status");
	}
}
