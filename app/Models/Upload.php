<?php

/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class Upload extends Model
{
    use SoftDeletes;

    protected $table = 'uploads';

    protected $hidden = [];

    protected $guarded = [];

    protected $dates = ['deleted_at'];

    /**
     * Get the user that owns upload.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get File path
     */
    public function path()
    {
        return url("files/" . $this->hash . "/" . $this->name);
    }

    public static function uploadFile($requestFileName)
    {
        $file = Input::file($requestFileName);

        // print_r($file);

        $folder = storage_path('uploads');
        $filename = $file->getClientOriginalName();

        $date_append = date("Y-m-d-His-");
        $upload_success = Input::file($requestFileName)->move($folder, $date_append . $filename);

        if ($upload_success) {

            // Get public preferences
            // config("laraadmin.uploads.default_public")
            $public = Input::get('public');
            if (isset($public)) {
                $public = true;
            } else {
                $public = false;
            }

            $upload = Upload::create([
                "name" => $filename,
                "path" => $folder . DIRECTORY_SEPARATOR . $date_append . $filename,
                "extension" => pathinfo($filename, PATHINFO_EXTENSION),
                "caption" => "",
                "hash" => "",
                "public" => $public,
                "user_id" => Auth::user()->id
            ]);
            // apply unique random hash to file
            while (true) {
                $hash = strtolower(str_random(20));
                if (!Upload::where("hash", $hash)->count()) {
                    $upload->hash = $hash;
                    break;
                }
            }
            $upload->save();
            return $upload;
        }
        return null;
        
    }
}
