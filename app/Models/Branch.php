<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Branch extends Model
{
    use SoftDeletes;
	
	protected $table = 'branches';
	
	protected $hidden = [
        
    ];

	protected $fillable = [
		"name",
		"prefix",
		"address",
		"email"
	];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function nextLocker() {
		$data = DB::select(DB::raw("select * from (select CAST(REPLACE(locker_number, b2.prefix, '') as UNSIGNED) as locker from clients c left join branches b2 on c.branch_id = b2.id  where branch_id = 1) T1 order by locker desc Limit 1"));
		return $data[0]->locker + 1;		
	}


	public static function userBranchId() {
		$user = Auth::user();
		switch($user->type)
		{
			case "Client":
				return $user->client->branch_id;
			case "Employee":
				return $user->employee->branch_id;
			default:
				return null;
		}
	}
}
