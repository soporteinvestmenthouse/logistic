<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use App\ModelLogic\ProjectLogic;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes, ProjectLogic;
	
	protected $table = 'projects';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function updates()
	{
		return $this->hasMany(Shipment_Update::class);
	}

	public function shipmentStatus()
	{
		return $this->belongsTo(Shipment_Update::class, "shipment_update_id");
	}
}
