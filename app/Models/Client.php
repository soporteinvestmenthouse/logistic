<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use App\ModelLogic\ClientLogic;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;
	use ClientLogic;
	
	protected $table = 'clients';
	
	protected $hidden = [
        
    ];

	protected $fillable = ["user_type_id", "name", "email", "branch_id", "locker_number", "address", "phone", "birthdate"];

	protected $guarded = [];

	protected $dates = ['deleted_at'];
}
