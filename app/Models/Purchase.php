<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use App\ModelLogic\PurchaseLogic;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    use SoftDeletes;
	use PurchaseLogic;
	
	protected $table = 'purchases';

	protected $fillable = [
        "status"
    ];
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	
	public function articles()
	{
		return $this->has_many(Purchase_Article::class);
	}

	public static function draft()
	{
		$new = self::create([
			"status" => "Draft"
		]);
		$new->save();
		return $new;		
	}

}
