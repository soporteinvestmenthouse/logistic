<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shipment extends Model
{
    use SoftDeletes;
	
	protected $table = 'shipments';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function purchases()
	{
		return $this->belongsToMany(Purchase::class, "purchaseshipments");
	}

	public function updates()
	{
		return $this->hasMany(Shipment_Update::class);
	}
}
