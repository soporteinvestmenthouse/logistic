<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use App\ModelLogic\WRLogic;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Warehouse_Receipt extends Model
{
    use SoftDeletes;
	use WRLogic;
	
	protected $table = 'warehouse_receipts';
	
	protected $hidden = [
        
    ];

	protected $fillable = [
        "status",
		"created_by",
		"updated_by",
		"project_id"
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	

}
