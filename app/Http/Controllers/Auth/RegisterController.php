<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Role;
use Eloquent;
use App\Models\Employee;
use App\Http\Controllers\Controller;
use App\Models\Client;
use DesarrollatuApp\NWCRM\Models\Module;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $roleCount = Role::count();
        return view('auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        Module::validateRules("Clients", $data);
        
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'type' => "Client",
        ]);

        $client = Client::create([
            "user_type_id", 
            "name" => $data['name'], 
            "email" => $data['email'], 
            "branch_id" => 1
        ]);
        
        
        $role = Role::where('name', 'CLIENTS')->first();
        $user->attachRole($role);
    
        return $user;
    }
}
