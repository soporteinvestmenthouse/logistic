<?php

/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Datatables;
use Collective\Html\FormFacade as Form;
use DesarrollatuApp\NWCRM\Models\Module;
use DesarrollatuApp\NWCRM\Models\ModuleFields;

use App\Models\Branch;
use App\Models\Employee;
use App\Models\Role;
use App\User;
use DesarrollatuApp\NWCRM\Helpers\LAHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BranchesController extends Controller
{
	public $show_action = true;
	public $view_col = 'name';
	public $listing_cols = ['id', 'name', 'email', 'address', 'prefix', 'upload_id'];

	public function __construct()
	{
		// Field Access of Listing Columns
		if (\DesarrollatuApp\NWCRM\Helpers\LAHelper::laravel_ver() >= 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Branches', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Branches', $this->listing_cols);
		}
	}

	/**
	 * Display a listing of the Branches.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Branches');

		if (Module::hasAccess($module->id)) {
			return View('la.branches.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Show the form for creating a new branch.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created branch in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if (Module::hasAccess("Branches", "create")) {

			$rules = Module::validateRules("Branches", $request);

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}

			try {
				DB::beginTransaction();
				$insert_id = Module::insert("Branches", $request);
				$branch = Branch::find($insert_id);
				$password = LAHelper::gen_password();

				// Create Employee
				$employee = Employee::create([
					'name' => $request->name,
					'email' => $request->email,
					'branch_id' => $insert_id,
					"designation" => $branch->name . " Owner"
				]);
				
				$branch->employee_id = $employee->id;
				$branch->save();

				// Create User
				$user = User::create([
					'name' => $request->name,
					'email' => $request->email,
					'password' => bcrypt($password),
					'context_id' => $employee->id,
					'type' => "Employee",
				]);

				// update user role
				$user->detachRoles();
				$role = Role::where("name", "MANAGER")->first();
				$user->attachRole($role);
				DB::commit();
			} catch (\Throwable $th) {
				DB::rollBack();
				throw $th;
			}

			return redirect()->route(config('laraadmin.adminRoute') . '.branches.index');
		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Display the specified branch.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if (Module::hasAccess("Branches", "view")) {

			$branch = Branch::find($id);
			if (isset($branch->id)) {
				$module = Module::get('Branches');
				$module->row = $branch;

				return view('la.branches.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('branch', $branch);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("branch"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Show the form for editing the specified branch.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if (Module::hasAccess("Branches", "edit")) {
			$branch = Branch::find($id);
			if (isset($branch->id)) {
				$module = Module::get('Branches');

				$module->row = $branch;

				return view('la.branches.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('branch', $branch);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("branch"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Update the specified branch in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if (Module::hasAccess("Branches", "edit")) {

			$rules = Module::validateRules("Branches", $request, true);

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}

			$insert_id = Module::updateRow("Branches", $request, $id);

			return redirect()->route(config('laraadmin.adminRoute') . '.branches.index');
		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Remove the specified branch from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if (Module::hasAccess("Branches", "delete")) {
			Branch::find($id)->delete();

			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.branches.index');
		} else {
			return redirect(config('laraadmin.adminRoute') . "/");
		}
	}

	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('branches')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Branches');

		for ($i = 0; $i < count($data->data); $i++) {
			for ($j = 0; $j < count($this->listing_cols); $j++) {
				$col = $this->listing_cols[$j];
				if ($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if ($col == $this->view_col && $data->data[$i][0] != 45) {
					$data->data[$i][$j] = '<a href="' . url(config('laraadmin.adminRoute') . '/branches/' . $data->data[$i][0]) . '/edit">' . $data->data[$i][$j] . '</a>';
				}
			}

			if ($this->show_action) {
				$output = '';
				if (Module::hasAccess("Branches", "edit") && $data->data[$i][0] != 45) {
					$output .= '<a href="' . url(config('laraadmin.adminRoute') . '/branches/' . $data->data[$i][0] . '/edit') . '" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}

				if (Module::hasAccess("Branches", "delete") && $data->data[$i][0] != 45) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.branches.destroy', $data->data[$i][0]], 'method' => 'delete', 'style' => 'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
