<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Client;
use App\Models\Purchase;
use App\Models\PurchaseShipment;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use DesarrollatuApp\NWCRM\Models\Module;
use DesarrollatuApp\NWCRM\Models\ModuleFields;

use App\Models\Shipment;

class ShipmentsController extends Controller
{
	public $show_action = true;
	public $view_col = 'purchase_id';
	public $listing_cols = ['id', 'from_store_id', 'to_store', 'travel_type_id', 'internal_code', 'status'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\DesarrollatuApp\NWCRM\Helpers\LAHelper::laravel_ver() >= 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Shipments', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Shipments', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Shipments.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Shipments');
		
		if(Module::hasAccess($module->id)) {
			return View('la.shipments.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new shipment.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Request $request)
	{
		if(Module::hasAccess("Shipments", "create")) {			
			$module = Module::get('Shipments');
				$purchase = Purchase::find($request->get("frompurchase", 0));				

				$client = $purchase != null ? Client::find($purchase->client) : null;
				return view('la.shipments.create', [
					'module' => $module,
					'view_col' => $this->view_col,
					'purchase' => $purchase,
					"client" => $client
				]);
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Store a newly created shipment in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Shipments", "create")) {
		
			$rules = Module::validateRules("Shipments", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}

			
			
			$insert_id = Module::insert("Shipments", $request);

			foreach(json_decode($request->get("purchases", "[]")) as $purchase_id)
			{
				PurchaseShipment::create([
					"shipment_id" => $insert_id,
					"purchase_id" => $purchase_id
				]);
			}
			
			return redirect()->route(config('laraadmin.adminRoute') . '.shipments.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified shipment.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Shipments", "view")) {
			
			$shipment = Shipment::find($id);
			if(isset($shipment->id)) {
				$module = Module::get('Shipments');
				$module->row = $shipment;
				
				return view('la.shipments.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('shipment', $shipment);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("shipment"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified shipment.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Shipments", "edit")) {			
			$shipment = Shipment::find($id);
			if(isset($shipment->id)) {	
				$module = Module::get('Shipments');
				
				$module->row = $shipment;
				
				return view('la.shipments.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('shipment', $shipment);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("shipment"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified shipment in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Shipments", "edit")) {
			
			$rules = Module::validateRules("Shipments", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Shipments", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.shipments.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified shipment from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Shipments", "delete")) {
			Shipment::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.shipments.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('shipments')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Shipments');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/shipments/'.$data->data[$i][0]).'/edit">'.$data->data[$i][$j].'</a>';
				}
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Shipments", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/shipments/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Shipments", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.shipments.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
