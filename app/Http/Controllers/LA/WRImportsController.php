<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use DesarrollatuApp\NWCRM\Models\Module;
use DesarrollatuApp\NWCRM\Models\ModuleFields;

use App\Models\WRImport;

class WRImportsController extends Controller
{
	public $show_action = true;
	public $view_col = 'file_id';
	public $listing_cols = ['id', 'imported_by', 'success', 'file_id'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\DesarrollatuApp\NWCRM\Helpers\LAHelper::laravel_ver() >= 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('WRImports', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('WRImports', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the WRImports.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('WRImports');
		
		if(Module::hasAccess($module->id)) {
			return View('la.wrimports.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new wrimport.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created wrimport in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
		if(Module::hasAccess("WRImports", "create")) {
			if ($request->hasFile('csv')) {
				$csv = $request->file('csv');
				$destinationPathFolder = storage_path("wrimports/");
				$csv->move($destinationPathFolder, "import-1.".$csv->getClientOriginalExtension());
				$path = $destinationPathFolder. "import-1.".$csv->getClientOriginalExtension();
				$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);				
				$sheetData = $spreadsheet->getActiveSheet()->toArray();
				$array = [];
				if (!empty($sheetData)) {
					for ($i=1; $i<count($sheetData); $i++) { //skipping first row
						$el = (object)[];
						$el->name = $sheetData[$i][0];
						$el->email = $sheetData[$i][1];
						$el->company = $sheetData[$i][2];
						$el->x	= $sheetData[$i][3];
						array_push($array, $el);
						break;
					}
				}
				return response()->json($array);				
			}
			
			return redirect()->route(config('laraadmin.adminRoute') . '.wrimports.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified wrimport.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("WRImports", "view")) {
			
			$wrimport = WRImport::find($id);
			if(isset($wrimport->id)) {
				$module = Module::get('WRImports');
				$module->row = $wrimport;
				
				return view('la.wrimports.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('wrimport', $wrimport);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("wrimport"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified wrimport.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("WRImports", "edit")) {			
			$wrimport = WRImport::find($id);
			if(isset($wrimport->id)) {	
				$module = Module::get('WRImports');
				
				$module->row = $wrimport;
				
				return view('la.wrimports.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('wrimport', $wrimport);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("wrimport"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified wrimport in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("WRImports", "edit")) {
			
			$rules = Module::validateRules("WRImports", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("WRImports", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.wrimports.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified wrimport from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("WRImports", "delete")) {
			WRImport::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.wrimports.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('wrimports')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('WRImports');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/wrimports/'.$data->data[$i][0]).'/edit">'.$data->data[$i][$j].'</a>';
				}
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("WRImports", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/wrimports/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("WRImports", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.wrimports.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
