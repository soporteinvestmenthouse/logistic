<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Branch;
use Auth;
use DB;
use Validator;
use Datatables;
use Entrust;
use Collective\Html\FormFacade as Form;
use DesarrollatuApp\NWCRM\Models\Module;
use DesarrollatuApp\NWCRM\Models\ModuleFields;

use App\Models\Project;
use App\Models\Shipment_Travel_Type;
use App\Models\Shipment_Update;
use Exception;

class ProjectsController extends Controller
{
	public $show_action = true;
	public $view_col = 'code';
	public $listing_cols = ['id', 'shipment_update_id', 'code', 'travel_type_id', 'estimated_departure', 'code_number', 'store_id', 'receive_store_id'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\DesarrollatuApp\NWCRM\Helpers\LAHelper::laravel_ver() >= 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Projects', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Projects', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Projects.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Projects');
		
		if(Module::hasAccess($module->id)) {
			return View('la.projects.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new project.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created project in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Projects", "create")) {
		
			$rules = Module::validateRules("Projects", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Projects", $request);
			
			$project = Project::find($insert_id);
			$type = Shipment_Travel_Type::find($project->travel_type_id);
			$number = Project::where("travel_type_id", $project->travel_type_id)->max("code_number") + 1;
			$project->code_number = $number;
			$project->code = $type->prefix.$number;
			$project->save();


			return redirect()->route(config('laraadmin.adminRoute') . '.projects.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified project.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Projects", "view")) {
			
			$project = Project::find($id);
			if(isset($project->id)) {
				$module = Module::get('Projects');
				$module->row = $project;
				
				return view('la.projects.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('project', $project);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("project"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified project.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Projects", "edit")) {			
			$project = Project::find($id);
			if(isset($project->id)) {	
				$module = Module::get('Projects');
				
				$module->row = $project;
				
				return view('la.projects.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('project', $project);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("project"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified project in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Projects", "edit")) {
			
			$rules = Module::validateRules("Projects", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Projects", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.projects.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified project from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Projects", "delete")) {
			Project::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.projects.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('projects')->select($this->listing_cols)->whereNull('deleted_at');
		if(!Entrust::hasRole('SUPER_ADMIN')) {
			$values = $values->where("branch_id", Branch::userBranchId());
		}
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Projects');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@") && !in_array($col, ["shipment_update_id"])) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/projects/'.$data->data[$i][0]).'/edit">'.$data->data[$i][$j].'</a>';
				}
				if($col == "shipment_update_id")
				{
					$su = Shipment_Update::find($data->data[$i][$j]);
					$data->data[$i][$j] =  $su != null ? '<div class="badge bg-'. $su->statusElement->color()  .'">'. $su->statusElement->name .'</div>' : "n/a";
				}
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Projects", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/projects/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Projects", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.projects.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
