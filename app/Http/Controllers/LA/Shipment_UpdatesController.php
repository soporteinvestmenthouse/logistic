<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use DesarrollatuApp\NWCRM\Models\Module;
use DesarrollatuApp\NWCRM\Models\ModuleFields;

use App\Models\Shipment_Update;

class Shipment_UpdatesController extends Controller
{
	public $show_action = true;
	public $view_col = 'project_id';
	public $listing_cols = ['id', 'project_id', 'message', 'status_at', 'status'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\DesarrollatuApp\NWCRM\Helpers\LAHelper::laravel_ver() >= 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Shipment_Updates', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Shipment_Updates', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Shipment_Updates.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		
		$module = Module::get('Shipment_Updates');
		
		if(Module::hasAccess($module->id)) {
			return View('la.shipment_updates.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new shipment_update.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created shipment_update in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	
		if(Module::hasAccess("Shipment_Updates", "create")) {
			
			$rules = Module::validateRules("Shipment_Updates", $request);					

			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Shipment_Updates", $request);
			
			return redirect()->back();
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified shipment_update.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Shipment_Updates", "view")) {
			
			$shipment_update = Shipment_Update::find($id);
			if(isset($shipment_update->id)) {
				$module = Module::get('Shipment_Updates');
				$module->row = $shipment_update;
				
				return view('la.shipment_updates.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('shipment_update', $shipment_update);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("shipment_update"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified shipment_update.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Shipment_Updates", "edit")) {			
			$shipment_update = Shipment_Update::find($id);
			if(isset($shipment_update->id)) {	
				$module = Module::get('Shipment_Updates');
				
				$module->row = $shipment_update;
				
				return view('la.shipment_updates.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('shipment_update', $shipment_update);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("shipment_update"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified shipment_update in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		
		if(Module::hasAccess("Shipment_Updates", "edit")) {
			
			$rules = Module::validateRules("Shipment_Updates", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Shipment_Updates", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.shipment_updates.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified shipment_update from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Shipment_Updates", "delete")) {
			Shipment_Update::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.shipment_updates.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('shipment_updates')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Shipment_Updates');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/shipment_updates/'.$data->data[$i][0]).'/edit">'.$data->data[$i][$j].'</a>';
				}
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Shipment_Updates", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/shipment_updates/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Shipment_Updates", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.shipment_updates.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
