<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Branch;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use DesarrollatuApp\NWCRM\Models\Module;
use DesarrollatuApp\NWCRM\Models\ModuleFields;

use App\Models\Client;
use Carbon\Carbon;

class ClientsController extends Controller
{
	public $show_action = true;
	public $view_col = 'name';
	public $listing_cols = ['id', 'user_type_id', 'name', 'email', 'user_id', 'branch_id', 'locker_number', 'address', 'phone', 'birthdate'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\DesarrollatuApp\NWCRM\Helpers\LAHelper::laravel_ver() >= 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Clients', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Clients', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Clients.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Clients');
		
		if(Module::hasAccess($module->id)) {
			return View('la.clients.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new client.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created client in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Clients", "create")) {
		
			$rules = Module::validateRules("Clients", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Clients", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.clients.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified client.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Clients", "view")) {
			
			$client = Client::find($id);
			if(isset($client->id)) {
				$module = Module::get('Clients');
				$module->row = $client;
				
				return view('la.clients.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('client', $client);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("client"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified client.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Clients", "edit")) {			
			$client = Client::find($id);
			if(isset($client->id)) {	
				$module = Module::get('Clients');
				
				$module->row = $client;
				
				return view('la.clients.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('client', $client);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("client"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified client in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Clients", "edit")) {
			
			$rules = Module::validateRules("Clients", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Clients", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.clients.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified client from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Clients", "delete")) {
			Client::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.clients.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('clients')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Clients');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col && $data->data[$i][0] != 2881) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/clients/'.$data->data[$i][0]).'/edit">'.$data->data[$i][$j].'</a>';
				}
			}
			
			if($this->show_action  ) {
				$output = '';
				if(Module::hasAccess("Clients", "edit") && $data->data[$i][0] != 2881) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/clients/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Clients", "delete") && $data->data[$i][0] != 2881) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.clients.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}

	public function import(Request $request)
	{
		if ($request->hasFile('csv')) {
			$csv = $request->file('csv');
			$csvContent = file_get_contents($csv->getRealPath());
			$array = array_map(function($v){return str_getcsv($v, ";");}, explode(PHP_EOL, $csvContent));
			array_shift($array);
			$usersData = [];
			set_time_limit(50000);

			//return response()->json($array);
			foreach($array as $csvUser)
			{
				try {
					$row = (object)[];			
					$prefixStr = preg_replace("/[0-9]+/", "", strval($csvUser[4]));
					$branch = Branch::where("prefix",  $prefixStr)->first();
					if($branch == null)
					{
						$branch = Branch::create([
							"name" => $csvUser[0],
							"prefix" => $prefixStr,
							"address" => "unknown",
							"email" => "unknown"
						]);
						array_push($usersData, ["email" => "unknown", "status" => "branch created", "reason" => "The imported branch " . $branch->name . " didn't exists and was created"]);					
					}
					$row->branch_id = $branch->id;
					$row->name = $csvUser[0];
					$row->address = $csvUser[8];
					$row->email = $csvUser[3];
					$row->phone = $csvUser[6];
					$row->locker_number = $csvUser[4];
					$row->created_at = Carbon::createFromFormat("d/m/Y H:i", $csvUser[14]);
					
					array_push($usersData, Client::checkAndCreate($row));
				} catch (\Throwable $th) {
					continue;
				}
			}

			return response()->json($usersData);
			
		}

		return redirect()->back();
	}
}
