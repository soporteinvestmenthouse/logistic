<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Upload;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use DesarrollatuApp\NWCRM\Models\Module;
use DesarrollatuApp\NWCRM\Models\ModuleFields;

use App\Models\Warehouse_Receipt;
use Illuminate\Support\Facades\Auth;

class Warehouse_ReceiptsController extends Controller
{
	public $show_action = true;
	public $view_col = 'wr';
	public $listing_cols = ['id', 'wr', 'status', 'shipper_id', 'consignee_id', 'description', 'internal_notes', 'extra_cost'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\DesarrollatuApp\NWCRM\Helpers\LAHelper::laravel_ver() >= 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Warehouse_Receipts', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Warehouse_Receipts', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Warehouse_Receipts.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Warehouse_Receipts');
		
		if(Module::hasAccess($module->id)) {
			return View('la.warehouse_receipts.index', [
				'show_actions' => $this->show_action, 
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new warehouse_receipt.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Request $request)
	{
		if(Module::hasAccess("Warehouse_Receipts", "create")) {
			
			$module = Module::get('Warehouse_Receipts');			
			$warehouse_receipt = new Warehouse_Receipt();
			$warehouse_receipt->status = "New";
			$warehouse_receipt->project_id = $request->get("project_id", null);
			$warehouse_receipt->created_by = Auth::user()->employee->id;
			$warehouse_receipt->updated_by = Auth::user()->employee->id;
			$warehouse_receipt->branch_id = Auth::user()->employee->branch_id;
			$module->row = $warehouse_receipt;
			if(Auth::user()->type == "Client")
			{
				return view('la.warehouse_receipts.declare', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('warehouse_receipt', $warehouse_receipt);
			}
			else {
				return view('la.warehouse_receipts.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('warehouse_receipt', $warehouse_receipt);
			}
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Store a newly created warehouse_receipt in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Warehouse_Receipts", "create")) {
			
			$rules = Module::validateRules("Warehouse_Receipts", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Warehouse_Receipts", $request);

			if($request->hasFile("bill_file"))
			{
				$wr = Warehouse_Receipt::find($insert_id);
				$upload = Upload::uploadFile("bill_file");
				if($upload != null)
				{
					
					$wr->bill = $upload->id;
					$wr->save();
				}
			}
			
			return redirect()->route(config('laraadmin.adminRoute') . '.warehouse_receipts.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified warehouse_receipt.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Warehouse_Receipts", "view")) {
			
			$warehouse_receipt = Warehouse_Receipt::find($id);
			if(isset($warehouse_receipt->id)) {
				$module = Module::get('Warehouse_Receipts');
				$module->row = $warehouse_receipt;
				
				return view('la.warehouse_receipts.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('warehouse_receipt', $warehouse_receipt);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("warehouse_receipt"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified warehouse_receipt.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Warehouse_Receipts", "edit")) {			
			$warehouse_receipt = Warehouse_Receipt::find($id);
			if(isset($warehouse_receipt->id)) {	
				$module = Module::get('Warehouse_Receipts');
				
				$module->row = $warehouse_receipt;
				if(Auth::user()->type == "Client")
				{
					return view('la.warehouse_receipts.declare', [
						'module' => $module,
						'view_col' => $this->view_col,
					])->with('warehouse_receipt', $warehouse_receipt);
				}
				else {
					return view('la.warehouse_receipts.edit', [
						'module' => $module,
						'view_col' => $this->view_col,
					])->with('warehouse_receipt', $warehouse_receipt);
				}
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("warehouse_receipt"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified warehouse_receipt in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Warehouse_Receipts", "edit")) {
			
			$rules = Module::validateRules("Warehouse_Receipts", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Warehouse_Receipts", $request, $id);
			
			if($request->hasFile("bill_file"))
			{
				$wr = Warehouse_Receipt::find($insert_id);
				$upload = Upload::uploadFile("bill_file");
				if($upload != null)
				{
					
					$wr->bill = $upload->id;
					$wr->save();
				}
			}

			return redirect()->back();
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified warehouse_receipt from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Warehouse_Receipts", "delete")) {
			Warehouse_Receipt::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.warehouse_receipts.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax($project_id = null)
	{
		$values = DB::table('warehouse_receipts')->select($this->listing_cols)->whereNull('deleted_at');
		if($project_id != null)
		{
			$values = $values->where("project_id", $project_id);
		}
		$out = Datatables::of($values)->make();
		$data = $out->getData();
		
		$fields_popup = ModuleFields::getModuleFields('Warehouse_Receipts');
		
		for($i=0; $i < count($data->data); $i++) {
			$currentStatus = "none";
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/warehouse_receipts/'.$data->data[$i][0]).'/edit">'.$data->data[$i][$j].'</a>';
				}

				switch($col)
				{
					case "status":
						$currentStatus = $data->data[$i][$j];
						$statusColors = [
							"Draft" => "default",
							"Receipt" => "success"
						];
						$status = array_key_exists($data->data[$i][$j], $statusColors) ? $statusColors[$data->data[$i][$j]] : "default";
						$data->data[$i][$j] = '<div class="badge bg-'.$status.'">'.$data->data[$i][$j].'</div>';
						break;
				}
			}
			
			if($this->show_action) {
				$output = '';
				/*if(Module::hasAccess("Shipments", "create") && $currentStatus == "Receipt") {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/warehouse_receipts/'.$data->data[$i][0].'/edit').'" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px; margin-right:3px">Generate Shipment</a>';
				}*/
				if(Module::hasAccess("Warehouse_Receipts", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/warehouse_receipts/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				if(Module::hasAccess("Warehouse_Receipts", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.warehouse_receipts.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
