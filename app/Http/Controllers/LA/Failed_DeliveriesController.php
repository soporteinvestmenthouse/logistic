<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use DesarrollatuApp\NWCRM\Models\Module;
use DesarrollatuApp\NWCRM\Models\ModuleFields;

use App\Models\Failed_Delivery;

class Failed_DeliveriesController extends Controller
{
	public $show_action = true;
	public $view_col = 'shipment_id';
	public $listing_cols = ['id', 'responsible_id', 'shipment_id', 'reason', 'image_reason_id'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\DesarrollatuApp\NWCRM\Helpers\LAHelper::laravel_ver() >= 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Failed_Deliveries', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Failed_Deliveries', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Failed_Deliveries.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Failed_Deliveries');
		
		if(Module::hasAccess($module->id)) {
			return View('la.failed_deliveries.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new failed_delivery.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created failed_delivery in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Failed_Deliveries", "create")) {
		
			$rules = Module::validateRules("Failed_Deliveries", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Failed_Deliveries", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.failed_deliveries.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified failed_delivery.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Failed_Deliveries", "view")) {
			
			$failed_delivery = Failed_Delivery::find($id);
			if(isset($failed_delivery->id)) {
				$module = Module::get('Failed_Deliveries');
				$module->row = $failed_delivery;
				
				return view('la.failed_deliveries.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('failed_delivery', $failed_delivery);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("failed_delivery"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified failed_delivery.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Failed_Deliveries", "edit")) {			
			$failed_delivery = Failed_Delivery::find($id);
			if(isset($failed_delivery->id)) {	
				$module = Module::get('Failed_Deliveries');
				
				$module->row = $failed_delivery;
				
				return view('la.failed_deliveries.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('failed_delivery', $failed_delivery);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("failed_delivery"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified failed_delivery in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Failed_Deliveries", "edit")) {
			
			$rules = Module::validateRules("Failed_Deliveries", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Failed_Deliveries", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.failed_deliveries.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified failed_delivery from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Failed_Deliveries", "delete")) {
			Failed_Delivery::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.failed_deliveries.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('failed_deliveries')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Failed_Deliveries');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/failed_deliveries/'.$data->data[$i][0]).'/edit">'.$data->data[$i][$j].'</a>';
				}
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Failed_Deliveries", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/failed_deliveries/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Failed_Deliveries", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.failed_deliveries.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
