<?php

namespace App\Http\Middleware;

use App\Models\Warehouse_Receipt;
use Closure;
use Entrust;
use Illuminate\Support\Facades\Route;

class NoDraftWRAllowed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        if($request->user() != null)
        {
            if(Route::currentRouteName() == config('laraadmin.adminRoute').".warehouse_receipts.update") return $next($request);

            if(!Entrust::hasRole("SUPER_ADMIN") && $request->user()->type == "Employee")
            {
                $wr = Warehouse_Receipt::where("created_by", $request->user()->employee->id)->where("status", "Draft")->first();
                if($wr != null && $request->url() != url(config('laraadmin.adminRoute') . '/warehouse_receipts/'.$wr->id.'/edit'))
                {
                    return redirect(config('laraadmin.adminRoute') . '/warehouse_receipts/'.$wr->id.'/edit');
                }
            }
            
        }
        return $next($request);
    }
}
