<?php
namespace App\ModelLogic;

use App\Models\Package;
use App\Models\Project;
use App\Models\Shipment;
use App\Models\Shipment_Agency;;
use Illuminate\Support\Facades\Auth;

trait WRLogic {
    public static function bootWRLogic()
    {
        static::created(function ($model) {
            if($model->project != null)
			{
				$model->branch_id = $model->project->branch_id;
				$model->store_id = $model->project->store_id;
				$model->save();
			}         
        });
    }

    public static function draft($project_id = null)
	{
		$new = self::create([
			"status" => "Draft",
			"created_by" => Auth::user()->employee->id,
			"updated_by" => Auth::user()->employee->id,
			"project_id" => $project_id
		]);
		$new->wr = $new->id;
		$new->save();
		return $new;		
	}

	public function project() {
		return $this->belongsTo(Project::class, "project_id");
	}

    public function shipment() {
		return $this->hasMany(Shipment::class, "shipment_id");
	}

	public function packages() {
		return $this->hasMany(Package::class, "whr_id");
	}

	public function agency() {
		return $this->belongsTo(Shipment_Agency::class, "sa_id");
	}

	public function readyToShip() {
		return $this->bill != null && $this->cost != null && $this->delivery_type != null && $this->payment_method != null;
	}

	public function calculateCost() {
		$cost = $this->packages()->sum("cost");
		$this->cost = $cost == 0 ? null : $cost;
		$this->save();
		return $this->cost;
	}
} 