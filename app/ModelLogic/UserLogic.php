<?php
namespace App\ModelLogic;

use Illuminate\Support\Str;

trait UserLogic 
{  
    public static function bootUserLogic()
    {
        static::created(function ($model) {            
            $model->renewApiKey();            
        });
    }

    public function renewApiKey()
    {
        $this->api_token = Str::random(60);
        $this->save();
    }
}