<?php
namespace App\ModelLogic;

use App\Models\Branch;
use App\Models\Role;
use App\Models\Warehouse_Receipt;
use App\User;
use Illuminate\Support\Str;
use Throwable;

trait ClientLogic 
{  
    public static function bootClientLogic()
    {
        static::created(function ($model) {
            $user = $model->user;
            if($user == null)
            {
                if($model->userByEmail)
                {
                    $model->userByEmail->context_id = $model->id;
                    $model->userByEmail->save();
                    $user = $model->userByEmail;
                }
                else 
                {
                    $passwordNew = env("TESTING_USERS", false) ? "A12345678" : Str::random(8);
                    $password = bcrypt($passwordNew);
                    $user = User::create([
                        "name" => $model->name,
                        "email" => $model->email,
                        "password" => $password,
                        "type" => "Client",
                        "context_id" => $model->id,
                        "api_token" => Str::random(32)
                    ]);                                        
                }
                
            }
            $role = Role::where('name', 'CLIENTS')->first();
            $user->attachRole($role);
            $model->checkAndCreateLocker();
            
        });

        static::saved(function ($model) {
            $model->checkAndCreateLocker();                        
        });
    }

    public function checkAndCreateLocker() {
        if($this->locker_number == null)
        {
            if(
                $this->address != null && $this->address != "" &&
                $this->phone != null && $this->phone != "" &&
                $this->birthdate != null && $this->birthdate != "" &&     
                $this->user_type_id != null               
            )
            {
                $this->createLocker();
            }   
        }
    }
    

    private function createLocker() {
        if($this->branch_id == null) $this->branch_id = 1; 
        $branch = \App\Models\Branch::find($this->branch_id);
        $this->locker_number = $branch->prefix . $branch->nextLocker();
        $this->save();
    }

    public function branch() {
        return $this->belongsTo(Branch::class, "branch_id");
    }

    public function user()
    {
        return $this->hasOne(User::class, "context_id");
    }

    public function userByEmail()
    {
        return $this->hasOne(User::class, "email", "email");
    }

    public static function checkAndCreate($data)
    {
        try 
        {
            self::create((array)$data);
            return ["email" => $data->email, "status" => "success", "reason" => "Imported"];
        }
        catch(Throwable $e)
        {
            if(isset($data->email))
            return ["email" => $data->email, "status" => "failed", "reason" => "This client already exists in the database."];
            else
            return ["email" => "unknown", "status" => "failed", "reason" => "Email is required: " . implode(",", (array)$data)];
        }
    }

    public function activeWr()
    {
        return $this->hasMany(Warehouse_Receipt::class, "consignee_id");
    }

    public function hasShipments() 
    {
        return $this->activeWr != null && count($this->activeWr) > 0;
    }
}