<?php
namespace App\ModelLogic;

use App\User;
use Illuminate\Support\Str;
use Throwable;

trait PurchaseLogic 
{  
    public static function bootPurchaseLogic()
    {
        static::created(function ($model) {
			$model->wr = $model->id;
			$model->save();
		});
    }    
}