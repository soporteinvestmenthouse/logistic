<?php
namespace App\ModelLogic;

use App\Models\Branch;
use App\User;
use Illuminate\Support\Str;
use Throwable;

trait ProjectLogic 
{  
    public static function bootProjectLogic()
    {
        static::created(function ($model) {
			$model->branch_id = Branch::userBranchId();
			$model->save();
		});
    }   
    
    
}