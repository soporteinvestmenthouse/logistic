<?php
namespace App\ModelLogic;

use App\Models\Project;

trait ShipmentUpdateLogic 
{  
    public static function bootShipmentUpdateLogic ()
    {
        static::created(function ($model) {
			$model->project->shipment_update_id = $model->id;
			$model->project->save();
		});
    }  
    
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}