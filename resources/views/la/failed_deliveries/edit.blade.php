@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/failed_deliveries') }}">@tslt("Failed Delivery")</a> :
@endsection
@section("contentheader_description", __t($failed_delivery->$view_col))
@section("section", __t("Failed Deliveries"))
@section("section_url", url(config('laraadmin.adminRoute') . '/failed_deliveries'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("Failed Deliveries Edit : ".$failed_delivery->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($failed_delivery, ['route' => [config('laraadmin.adminRoute') . '.failed_deliveries.update', $failed_delivery->id ], 'method'=>'PUT', 'id' => 'failed_delivery-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'responsible_id')
					@la_input($module, 'shipment_id')
					@la_input($module, 'reason')
					@la_input($module, 'image_reason_id')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/failed_deliveries') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#failed_delivery-edit-form").validate({
		
	});
});
</script>
@endpush
