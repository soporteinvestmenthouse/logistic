@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/shipment_travel_types') }}">@tslt("Shipment Travel Type")</a> :
@endsection
@section("contentheader_description", __t($shipment_travel_type->$view_col))
@section("section", __t("Shipment Travel Types"))
@section("section_url", url(config('laraadmin.adminRoute') . '/shipment_travel_types'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("Shipment Travel Types Edit : ".$shipment_travel_type->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($shipment_travel_type, ['route' => [config('laraadmin.adminRoute') . '.shipment_travel_types.update', $shipment_travel_type->id ], 'method'=>'PUT', 'id' => 'shipment_travel_type-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/shipment_travel_types') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#shipment_travel_type-edit-form").validate({
		
	});
});
</script>
@endpush
