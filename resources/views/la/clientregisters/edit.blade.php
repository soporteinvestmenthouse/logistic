@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/clientregisters') }}">@tslt("ClientRegister")</a> :
@endsection
@section("contentheader_description", __t($clientregister->$view_col))
@section("section", __t("ClientRegisters"))
@section("section_url", url(config('laraadmin.adminRoute') . '/clientregisters'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("ClientRegisters Edit : ".$clientregister->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($clientregister, ['route' => [config('laraadmin.adminRoute') . '.clientregisters.update', $clientregister->id ], 'method'=>'PUT', 'id' => 'clientregister-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'email')
					@la_input($module, 'password')
					@la_input($module, 'password_confirm')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/clientregisters') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#clientregister-edit-form").validate({
		
	});
});
</script>
@endpush
