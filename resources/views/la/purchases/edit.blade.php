@extends("la.layouts.app")

@section('contentheader_title')
    <a href="{{ url(config('laraadmin.adminRoute') . '/purchases') }}">@tslt("Purchase")</a> :
@endsection
@section('contentheader_description', __t($purchase->$view_col))
@section('section', __t('Purchases'))
@section('section_url', url(config('laraadmin.adminRoute') . '/purchases'))
@section('sub_section', 'Edit')

@section('htmlheader_title', __t('Purchases Edit : ' . $purchase->$view_col))

@section('main-content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box">
        <div class="box-header">

        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-8 col-md-offset-2" id="view">
                    <div class="row">
                        {!! Form::model($purchase, ['route' => [config('laraadmin.adminRoute') . '.purchases.update', $purchase->id ], 'method'=>'PUT', 'id' => 'purchase-edit-form']) !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        @la_input($module, 'client')
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        @la_input($module, 'shipment_agency_id')
                                    </div>
                                    <div class="col-md-6">
                                        @la_input($module, 'external_order_no')
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Articles</h3>
                                        <table class="table table-bordered">
                                            <tr class="success">
                                                <th>QTY</th>
                                                <th>Description</th>
                                                <th>Unit. Price</th>
                                                <th>Total</th>
                                                <th>Tracking</th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        @la_input($module, 'notes')
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="label-control">Sub-Total</label>
                                            <input type="text" class="form-control" disabled
                                                value="0.0">
                                        </div>
                                        <div class="form-group">
                                            <label class="label-control">Purchase Value</label>
                                            <input type="text" class="form-control" disabled
                                                value="0.0">
                                        </div>
                                        <div class="form-group">
                                            <label class="label-control">Total</label>
                                            <input type="text" class="form-control" disabled
                                                value="0.0">
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <h4>Internal Use</h4>

                                <div class="row">
                                    <div class="col-md-6">
                                        @la_input($module, 'credit_card')
                                        @la_input($module, 'payment_status')
                                        @la_input($module, 'payment_method')
                                        @la_input($module, 'payment_date_at')
                                        @la_input($module, 'cc_payment_at')
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    {!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/purchases') }}">@tslt("Cancel")</a></button>
                                </div>
				        {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-md-8 col-md-offset-2" style="display:none" id="edit">

                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
    </script>
@endpush
