@extends('la.layouts.app')

@section('htmlheader_title') @tslt("Dashboard") @endsection
@section('contentheader_title') @tslt("Dashboard") @endsection
@section('contentheader_description') @tslt("Organisation Overview") @endsection

@section('main-content')
  @include("la.".strtolower(Auth::user()->type)."s.dashboard")
@endsection




