@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/packages') }}">@tslt("Package")</a> :
@endsection
@section("contentheader_description", __t($package->$view_col))
@section("section", __t("Packages"))
@section("section_url", url(config('laraadmin.adminRoute') . '/packages'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("Packages Edit : ".$package->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($package, ['route' => [config('laraadmin.adminRoute') . '.packages.update', $package->id ], 'method'=>'PUT', 'id' => 'package-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'whr_id')
					@la_input($module, 'height')
					@la_input($module, 'width')
					@la_input($module, 'length')
					@la_input($module, 'units')
					@la_input($module, 'weight')
					@la_input($module, 'package_type_id')
					@la_input($module, 'description')
					@la_input($module, 'cost')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/packages') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#package-edit-form").validate({
		
	});
});
</script>
@endpush
