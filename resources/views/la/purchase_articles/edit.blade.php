@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/purchase_articles') }}">@tslt("Purchase Article")</a> :
@endsection
@section("contentheader_description", __t($purchase_article->$view_col))
@section("section", __t("Purchase Articles"))
@section("section_url", url(config('laraadmin.adminRoute') . '/purchase_articles'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("Purchase Articles Edit : ".$purchase_article->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($purchase_article, ['route' => [config('laraadmin.adminRoute') . '.purchase_articles.update', $purchase_article->id ], 'method'=>'PUT', 'id' => 'purchase_article-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'units')
					@la_input($module, 'description')
					@la_input($module, 'unit_price')
					@la_input($module, 'tracking')
					@la_input($module, 'purchase_id')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/purchase_articles') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#purchase_article-edit-form").validate({
		
	});
});
</script>
@endpush
