@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/claim_types') }}">@tslt("Claim Type")</a> :
@endsection
@section("contentheader_description", __t($claim_type->$view_col))
@section("section", __t("Claim Types"))
@section("section_url", url(config('laraadmin.adminRoute') . '/claim_types'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("Claim Types Edit : ".$claim_type->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($claim_type, ['route' => [config('laraadmin.adminRoute') . '.claim_types.update', $claim_type->id ], 'method'=>'PUT', 'id' => 'claim_type-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/claim_types') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#claim_type-edit-form").validate({
		
	});
});
</script>
@endpush
