@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/shipments') }}">@tslt("Shipment")</a> :
@endsection
@section("contentheader_description", __t($shipment->$view_col))
@section("section", __t("Shipments"))
@section("section_url", url(config('laraadmin.adminRoute') . '/shipments'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("Shipments Edit : ".$shipment->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8">
				<div class="col-md-12 no-padding">
					<h4 class="pull-left">Purchases on this shipment</h4>
					<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">@tslt("Add") @tslt("Shipment Update")</button>
				</div>
				<table class="table">
					<thead>
						<tr>
							<th>@tslt("Status")</th>
							<th>@tslt("Date")</th>
							<th>@tslt("Message")</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($shipment->updates as $update)
							<tr>
								<td><div class="btn btn-xs btn-{{ $update->statusElement->color() }}">{{ $update->statusElement->name }}</div></td>
								<td>{{ \Carbon\Carbon::parse($update->status_at)->format("d/m/Y H:i") }}</td>						
								<td>{{ $update->message }}</td>	
							</tr>
						@endforeach
					</tbody>
				</table>

				<!-- Chat box -->
              <div class="box box-success">
                <div class="box-header">
                  <i class="fa fa-comments-o"></i>
                  <h3 class="box-title">Chat</h3>
                  <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                    <div class="btn-group" data-toggle="btn-toggle" >
                      <button type="button" class="btn btn-default btn-sm active"><i class="fa fa-square text-green"></i></button>
                      <button type="button" class="btn btn-default btn-sm"><i class="fa fa-square text-red"></i></button>
                    </div>
                  </div>
                </div>
                <div class="box-body chat" id="chat-box">
                  <!-- chat item -->
                  <div class="item">
                    <img src="{{asset('/la-assets/img/user4-128x128.jpg')}}" alt="user image" class="online">
                    <p class="message">
                      <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
                        Mike Doe
                      </a>
                      I would like to meet you to discuss the latest news about
                      the arrival of the new theme. They say it is going to be one the
                      best themes on the market
                    </p>
                    <div class="attachment">
                      <h4>Attachments:</h4>
                      <p class="filename">
                        Theme-thumbnail-image.jpg
                      </p>
                      <div class="pull-right">
                        <button class="btn btn-primary btn-sm btn-flat">Open</button>
                      </div>
                    </div><!-- /.attachment -->
                  </div><!-- /.item -->
                  <!-- chat item -->
                  <div class="item">
                    <img src="{{asset('/la-assets/img/user3-128x128.jpg')}}" alt="user image" class="offline">
                    <p class="message">
                      <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:15</small>
                        Alexander Pierce
                      </a>
                      I would like to meet you to discuss the latest news about
                      the arrival of the new theme. They say it is going to be one the
                      best themes on the market
                    </p>
                  </div><!-- /.item -->
                  <!-- chat item -->
                  <div class="item">
                    <img src="{{asset('/la-assets/img/user-placeholder.png')}}" alt="user image" class="offline">
                    <p class="message">
                      <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:30</small>
                        Susan Doe
                      </a>
                      I would like to meet you to discuss the latest news about
                      the arrival of the new theme. They say it is going to be one the
                      best themes on the market
                    </p>
                  </div><!-- /.item -->
                </div><!-- /.chat -->
                <div class="box-footer">
                  <div class="input-group">
                    <input class="form-control" placeholder="Type message...">
                    <div class="input-group-btn">
                      <button class="btn btn-success"><i class="fa fa-plus"></i></button>
                    </div>
                  </div>
                </div>
              </div><!-- /.box (chat box) -->
			</div>
			<div class="col-md-4">
				<h4>WR on this shipment</h4>
					<table class="table">
						<thead>
							<tr>
								<th>@tslt("WR")</th>
								<th>@tslt("Contents")</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($shipment->purchases as $purch)
								<tr>
									<td>{{ $purch->id }}</td>
									<td>{{ $purch->content_description }}</td>						
								</tr>
							@endforeach
						</tbody>
					</table>
				<hr>
				{!! Form::model($shipment, ['route' => [config('laraadmin.adminRoute') . '.shipments.update', $shipment->id ], 'method'=>'PUT', 'id' => 'shipment-edit-form']) !!}
					
					
					@la_input($module, 'from_store_id')
					@la_input($module, 'to_store')
					@la_input($module, 'travel_type_id')
					@la_input($module, 'internal_code')
					@la_input($module, 'status')
					{{--
					@la_input($module, 'purchase_id')
					@la_input($module, 'previous_shipment_id')
					@la_form($module)
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/shipments') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}

				
				
			</div>
		</div>
	</div>
</div>

@php $shipmentModule = $module; $module = \DesarrollatuApp\NWCRM\Models\Module::get('Shipment_Updates'); @endphp
@la_access("Shipment_Updates", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">@tslt("Add") @tslt("Shipment Update")</h4>
			</div>
			{!! Form::open(['action' => 'LA\Shipment_UpdatesController@store', 'id' => 'shipment_update-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                   {{-- @la_form($module)--}}
					
					{{--
					@la_input($module, 'shipment_id')--}}
					<input type="hidden" value="{{$shipment->id}}" name="shipment_id">
					@la_input($module, 'message')
					@la_input($module, 'status_at')
					@la_input($module, 'status')
					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">@tslt("Close")</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@php $module = $shipmentModule; @endphp
@endla_access
@endsection

@push('scripts')
<script>
$(function () {
	$("#shipment-edit-form").validate({
		
	});
});
</script>
@endpush
