@extends("la.layouts.app")

@section('contentheader_title')
    <a href="{{ url(config('laraadmin.adminRoute') . '/shipments') }}">@tslt("Shipment")</a> :
@endsection
@section('contentheader_description')
@section('section', __t('Shipments'))
@section('section_url', url(config('laraadmin.adminRoute') . '/shipments'))
@section('sub_section', 'Create')

@section('htmlheader_title', __t('Shipments Create'))

@section('main-content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box">
        <div class="box-header">

        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    {!! Form::open(['action' => 'LA\ShipmentsController@store', 'id' => 'shipment-add-form']) !!}

                    @if ($client != null)
                        @la_input($module, 'client_id', $client->id)
                    @else
                        @la_input($module, 'client_id')
                    @endif


                    @if ($purchase != null)
                        <h3>Consolidated Purchase(s)</h3>
                        <p><i>Available purchases to consolidate for client {{$client->name}} <b>{{$client->locker_number}} are shown in table</b></i></p>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>@tslt("WR")</th>
                                    <th>@tslt("Content")</th>
                                    <th>@tslt("Action")</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $purchase->id }}</td>
                                    <td>{{ $purchase->content_description }}</td>
                                    <td><button type="button" id="addPurchase{{$purchase->id}}" onclick="addPurchase({{$purchase->id}})" class="btn btn-success btn-xs" style="display:none">@tslt("Add")</button><button  type="button"  id="removePurchase{{$purchase->id}}" onclick="removePurchase({{$purchase->id}})" class="btn btn-danger btn-xs">@tslt("Remove")</button></td>
                                </tr>
                            </tbody>
                        </table>

                    @endif

                    {{-- @la_form($module)
                    @la_input($module, 'previous_shipment_id')
                    @la_input($module, 'internal_code')
					@la_input($module, 'status') --}}

                    @la_input($module, 'from_store_id')
                    @la_input($module, 'to_store')
                    @la_input($module, 'travel_type_id')


                    
                    {!! Form::submit('Submit', ['class' => 'btn btn-success']) !!}

                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        function removeAllElements(array, elem) {
            var index = array.indexOf(elem);
            while (index > -1) {
                array.splice(index, 1);
                index = array.indexOf(elem);
            }
        }

        function getPurchasesValue()
        {
            return JSON.parse($("#purchases-input").val());
        }

        function setPurchasesValue(arr)
        {
            $("#purchases-input").val(JSON.stringify(arr))
            console.log("current value of purchases-input", arr)
            
        }

        function addPurchase(id)
        {
            let values = getPurchasesValue();
            removeAllElements(values, id);
            values.push(id);
            setPurchasesValue(values)
            $("#addPurchase" + id).hide();
            $("#removePurchase" + id).show();
        }

        function removePurchase(id)
        {
            let values = getPurchasesValue();
            removeAllElements(values, id);
            setPurchasesValue(values)
            $("#addPurchase" + id).show();
            $("#removePurchase" + id).hide();
        }
    </script>
@endpush
