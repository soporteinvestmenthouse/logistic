@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/shipment_agencies') }}">@tslt("Shipment Agency")</a> :
@endsection
@section("contentheader_description", __t($shipment_agency->$view_col))
@section("section", __t("Shipment Agencies"))
@section("section_url", url(config('laraadmin.adminRoute') . '/shipment_agencies'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("Shipment Agencies Edit : ".$shipment_agency->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($shipment_agency, ['route' => [config('laraadmin.adminRoute') . '.shipment_agencies.update', $shipment_agency->id ], 'method'=>'PUT', 'id' => 'shipment_agency-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'logo_id')
					@la_input($module, 'tracking_url')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/shipment_agencies') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#shipment_agency-edit-form").validate({
		
	});
});
</script>
@endpush
