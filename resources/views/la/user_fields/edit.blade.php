@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/user_fields') }}">@tslt("User Field")</a> :
@endsection
@section("contentheader_description", __t($user_field->$view_col))
@section("section", __t("User Fields"))
@section("section_url", url(config('laraadmin.adminRoute') . '/user_fields'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("User Fields Edit : ".$user_field->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($user_field, ['route' => [config('laraadmin.adminRoute') . '.user_fields.update', $user_field->id ], 'method'=>'PUT', 'id' => 'user_field-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'type')
					@la_input($module, 'user_type_id')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/user_fields') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#user_field-edit-form").validate({
		
	});
});
</script>
@endpush
