@extends("la.layouts.app")

@section('contentheader_title')
    <a href="{{ url(config('laraadmin.adminRoute') . '/warehouse_receipts') }}">@tslt("Declare Shipment")</a> :
@endsection
@section('contentheader_description', __t($warehouse_receipt->$view_col))
@section('section', __t('Declare Shipments'))
@section('section_url', url(config('laraadmin.adminRoute') . '/warehouse_receipts'))
@section('sub_section', 'Edit')

@section('htmlheader_title', __t('Declare Shipment: ' . $warehouse_receipt->$view_col))

@section('main-content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box">
        <div class="box-header">
		 <div class="row">
		<div class="col-md-10 col-md-offset-1">
			<button class="btn btn-{{$warehouse_receipt->status == 'Draft' ? 'default' : 'primary'}}">{{$warehouse_receipt->status}}</button>
			</div>
			</div>
        </div>
        <div class="box-body">
            {!! Form::model($warehouse_receipt, ['route' => [config('laraadmin.adminRoute') . '.warehouse_receipts.update', $warehouse_receipt->id], 'method' => 'PUT', 'id' => 'warehouse_receipt-edit-form', "enctype" => "multipart/form-data"]) !!}
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="label-control">WR</label>
                                <input type="text" class="form-control" disabled
                                    value="00000{{ $warehouse_receipt->wr }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="label-control">Created</label>
                                <input type="text" class="form-control" disabled
                                    value="{{ $warehouse_receipt->created_at }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="label-control">Weight-Vol</label>
                                <input value="{{ $warehouse_receipt->packages()->sum("weight_vol")}}" type="number" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="label-control">Agency</label>
                           <input type="text" class="form-control" disabled
                                    value="{{ $warehouse_receipt->agency->name }}">
                        </div>
                    </div>
                    <div class="row">
                        
						<div class="col-md-4">
                            @la_input($module, 'receiver_name', null, null, "form-control general_input")
                        </div>
                    </div>
                    <p><strong>1</strong> Debe de subir la factura del envío </p>

                    <div class="row">
                        <div class="col-md-6">
                            @if($warehouse_receipt->bill == "[]")
                                <label class="label-control">Upload Bill <small>Only PDF or Image</small></label>
                                <input name="bill_file" type="file" accept="application/pdf, image/*">
                            @else
                                <p><i class="fa fa-check" style="color:green"></i> Done</p>
                            @endif
                        </div>
                    </div>
                    <br>
                    <p><strong>2</strong> Debe declarar el monto del envío</p>
                    <div class="row">
                        <div class="col-md-6">
                            @if($warehouse_receipt->cost == null)
                                @la_input($module, 'cost', null, null, "form-control general_input")
                            @else
                                <p><i class="fa fa-check" style="color:green"></i> Done <br> <strong>Declared:</strong> ${{number_format($warehouse_receipt->cost, 2)}}</p>

                            @endif
                            
                        </div>
                    </div>
                     <p><strong>3</strong> Elija un tipo de pago</p>
                    <div class="row">
                        <div class="col-md-6">
                            @la_input($module, 'payment_method', null, null, "form-control general_input")
                            
                        </div>
                    </div>
                     <p><strong>4</strong> ¿Cómo desea retirar?</p>
                    <div class="row">
                        <div class="col-md-6">
                            @la_input($module, 'delivery_type', null, null, "form-control general_input")
                            
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
					
                        @if($warehouse_receipt->status == "Draft")
							{!! Form::submit('Save as Draft', ["id" => "submit-general-draft", 'class' => 'btn btn-default']) !!} 
						@endif
						{!! Form::submit($warehouse_receipt->status == "Draft" ? 'Create Receipt' : "Update", ["id" => "submit-general", 'class' => 'btn btn-success']) !!} <button class="btn btn-default pull-right"><a
                                href="{{ url(config('laraadmin.adminRoute') . '/warehouse_receipts') }}">@tslt("Close")</a></button>
								
					
                    </div>
                    
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection



@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>

@endpush

@push('styles')
	<style>
		.hide-labels label {
			display:none
		}

		#warehouse_receipt-edit-form > .form-control {
			display:none
		}
	</style>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush