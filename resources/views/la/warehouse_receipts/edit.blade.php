@extends("la.layouts.app")

@section('contentheader_title')
    <a href="{{ url(config('laraadmin.adminRoute') . '/warehouse_receipts') }}">@tslt("Warehouse Receipt")</a> :
@endsection
@section('contentheader_description', __t($warehouse_receipt->$view_col))
@section('section', __t('Warehouse Receipts'))
@section('section_url', url(config('laraadmin.adminRoute') . '/warehouse_receipts'))
@section('sub_section', 'Edit')

@section('htmlheader_title', __t('Warehouse Receipts Edit : ' . $warehouse_receipt->$view_col))

@section('main-content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box">
        <div class="box-header">
		 <div class="row">
		<div class="col-md-10 col-md-offset-1">
			<button class="btn btn-{{$warehouse_receipt->status == 'Draft' ? 'default' : 'primary'}}">{{$warehouse_receipt->status}}</button>
			</div>
			</div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    
                    <div class="row">
						<div class="col-md-3">
                            @la_input($module, 'branch_id', null, null, "form-control general_input")
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="label-control">WR</label>
                                <input type="text" class="form-control" disabled placeholder="Set on Save"
                                    value="{{ $warehouse_receipt->wr }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="label-control">Created</label>
                                <input type="text" class="form-control" disabled
                                    value="{{ $warehouse_receipt->created_at }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            @la_input($module, 'created_by', null, null, "form-control general_input")
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            @la_input($module, 'store_id', null, null, "form-control general_input")
                        </div>
                        <div class="col-md-4">
                            @la_input($module, 'sa_id', null, null, "form-control general_input")
                        </div>
                        <div class="col-md-4">
                            @la_input($module, 'tracking_agency_code', null, null, "form-control general_input")
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            @la_input($module, 'consignee_id', 2881, null, "form-control general_input")
                        </div>
						<div class="col-md-3">
                            @la_input($module, 'receiver_name', null, null, "form-control general_input")
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
							<h5>Packages</h5>
                            <table id="packages_table" class="table table-bordered">
                                <thead  >
                                    <tr class="success">
                                        <th>Length</th>
                                        <th>Width</th>
                                        <th>Height</th>
                                        <th>Units</th>
                                        <th>Weight</th>
										<th>Weight-Vol</th>
                                        <th>Type</th>
                                        <th>Description</th>
                                        <th>Cost</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>	
									@foreach ($warehouse_receipt->packages as $package)	
									<tr>
																				
										<td>{{$package->length}}</td>
										<td>{{$package->width}}</td>
										<td>{{$package->height}}</td>
										<td>{{$package->units}}</td>
										<td>{{$package->weight}}</td>
										<td>{{$package->weight_vol}}</td>
										<td>{{$package->package_type_id}}</td>
										<td>{{$package->description}}</td>
										<td>{{$package->cost}}</td>
										<td>
										@la_access("Packages", "edit")
										<a href="{{url(config('laraadmin.adminRoute') . '/packages/'.$package->id.'/edit')}}" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
										@endla_access
										@la_access("Packages", "delete")
										
										{!! Form::open(['route' => [config('laraadmin.adminRoute') . '.packages.destroy', $package->id], 'method' => 'delete', 'style'=>'display:inline']) !!}
											<button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>
											{!!Form::close()!!}
											@endla_access
										</td>										
																		
									</tr>
									@endforeach
                                </tbody>
								<tfoot>
									@php
										$packageModule = \DesarrollatuApp\NWCRM\Models\Module::get('Packages')
									@endphp
                                    @la_access("Packages", "create")
                                    <tr class="hide-labels">
                                        {!! Form::open(['action' => 'LA\PackagesController@store', 'id' => 'package-add-form']) !!}
										<input type="hidden" name="whr_id" value="{{$warehouse_receipt->id}}">
										<td>@la_input($packageModule, 'length')</td>
										<td>@la_input($packageModule, 'width')</td>
										<td>@la_input($packageModule, 'height')</td>
										<td>@la_input($packageModule, 'units')</td>
										<td>@la_input($packageModule, 'weight')</td>
										<td>@la_input($packageModule, 'weight_vol')</td>
										<td>@la_input($packageModule, 'package_type_id')</td>
										<td>@la_input($packageModule, 'description')</td>
										<td>@la_input($packageModule, 'cost')</td>
										<td><button type="button" class="btn btn-success" id="submitpackage">Add</button></td>										

                                    </tr>
                                    {!! Form::close() !!}
                                    @endla_access
								</tfoot>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="label-control">Pieces</label>
                                <input type="number" class="form-control" readonly value="{{ $warehouse_receipt->packages()->count() }}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="label-control">Weight</label>
                                <input type="number" class="form-control" readonly value="{{ $warehouse_receipt->packages()->sum("weight") }}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="label-control">Weight-Vol</label>
                                <input value="{{ $warehouse_receipt->packages()->sum("weight_vol")}}" type="number" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">

                            <div class="form-group">
                                <label class="label-control">Volume</label>
                                <input type="number" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="label-control">Cost</label>
                                <input type="number" class="form-control" readonly value="{{ $warehouse_receipt->calculateCost() }}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            @la_input($module, 'extra_cost', null, null, "form-control general_input")
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            @la_input($module, 'description', null, null, "form-control general_input")
                        </div>
                        <div class="col-md-6">
                            @la_input($module, 'internal_notes', null, null, "form-control general_input")
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            @la_input($module, 'invoice_included', null, null, "form-control general_input")
                        </div>
                        <div class="col-md-6">
                            @la_input($module, 'bill', null, null, "form-control general_input")
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                    @if(isset($warehouse_receipt->id))
					{!! Form::model($warehouse_receipt, ['route' => [config('laraadmin.adminRoute') . '.warehouse_receipts.update', $warehouse_receipt->id], 'method' => 'PUT', 'id' => 'warehouse_receipt-edit-form']) !!}
                    @else
                    {!! Form::model($warehouse_receipt, ['route' => config('laraadmin.adminRoute') . '.warehouse_receipts.store', 'method' => 'POST', 'id' => 'warehouse_receipt-edit-form']) !!}
                    @endif
                        @if($warehouse_receipt->status == "Draft")
							{!! Form::submit('Save as Draft', ["id" => "submit-general-draft", 'class' => 'btn btn-default']) !!} 
						@endif
						{!! Form::submit($warehouse_receipt->status == "Draft" ? 'Create Receipt' : "Update", ["id" => "submit-general", 'class' => 'btn btn-success']) !!} <button class="btn btn-default pull-right"><a
                                href="{{ url(config('laraadmin.adminRoute') . '/warehouse_receipts') }}">@tslt("Close")</a></button>
								
					{!! Form::close() !!}
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

@endsection



@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
    <script>
        $(function() {
			$("#submit-general").click(function() {
				event.preventDefault();
				$(".general_input").each(function(e) {
					$("#warehouse_receipt-edit-form").append(this);
				})
				$("#warehouse_receipt-edit-form").append('<input type="hidden" name="status" value="Receipt" />')
				$("#warehouse_receipt-edit-form").submit();
			});

			$("#submit-general-draft").click(function() {
				event.preventDefault();
				$(".general_input").each(function(e) {
					$("#warehouse_receipt-edit-form").append(this);
				})
				$("#warehouse_receipt-edit-form").submit();
			});
			
            $("#warehouse_receipt-edit-form").validate({
				
            });
			$("#submitpackage").on('click', function() {
				$('#package-add-form').submit();
			})
			$("[name='consignee_id']").change(function(e) {
				if($(this).val() != 2881)
					$("[name='receiver_name']").val($(this).find("option:selected").text())
				else
					$("[name='receiver_name']").val("")
			})
        });

        @if($warehouse_receipt->Status == "Draft")
        (function($) {
	$('body').pgNotification({
		style: 'circle',
		title: 'LaraAdmin',
		message: "You must finish the WR <br /> to continue",
		position: "top-right",
		timeout: 0,
		type: "warning",
		thumbnail: '<img width="40" height="40" style="display: inline-block;" src="{{ Gravatar::fallback(asset('la-assets/img/user-placeholder.png'))->get(Auth::user()->email, 'default') }}" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">'
	}).show();
})(window.jQuery);
        @endif
    </script>
@endpush

@push('styles')
	<style>
		.hide-labels label {
			display:none
		}

		#warehouse_receipt-edit-form > .form-control {
			display:none
		}
	</style>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush