@extends("la.layouts.app")

@section("contentheader_title", __t("Warehouse Receipts"))
@section("contentheader_description", __t("Warehouse Receipts listing"))
@section("section", __t("Warehouse Receipts"))
@section("sub_section", __t("Listing"))
@section("htmlheader_title", __t("Warehouse Receipts Listing"))

@section("headerElems")
@la_access("Warehouse_Receipts", "create")
	<a href="{{url(config('laraadmin.adminRoute') . '/warehouse_receipts/create')}}" class="btn btn-success btn-sm pull-right" >@tslt("Add") @tslt("Warehouse Receipt")</a>
@endla_access
@la_access("WRImports", "create")
	<button class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#AddWRImportModal">@tslt("Import")</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ isset($module->fields[$col]['label']) ? __t($module->fields[$col]['label']) : __t(ucfirst($col)) }}</th>
			@endforeach
			@if($show_actions)
			<th>@tslt("Actions")</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Warehouse_Receipts", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">@tslt("Add") @tslt("Warehouse Receipt")</h4>
			</div>
			{!! Form::open(['action' => 'LA\Warehouse_ReceiptsController@store', 'id' => 'warehouse_receipt-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    @la_form($module)
					
					{{--
					@la_input($module, 'shipper_id')
					@la_input($module, 'consignee_id')
					@la_input($module, 'description')
					@la_input($module, 'zone')
					@la_input($module, 'invoice_included')
					@la_input($module, 'internal_notes')
					@la_input($module, 'extra_cost')
					@la_input($module, 'bill')
					@la_input($module, 'wr')
					--}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">@tslt("Close")</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access
@la_access("WRImports", "create")
@php $importModule = \DesarrollatuApp\NWCRM\Models\Module::get('WRImports') @endphp
<div class="modal fade" id="AddWRImportModal" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">@tslt("Add") @tslt("WRImport")</h4>
			</div>
			{!! Form::open(['action' => 'LA\WRImportsController@store', 'id' => 'wr-import-form', "enctype" => "multipart/form-data"]) !!}
			<div class="box-body">
				<div class="form-group">
					<label for="csv" class="label-control">@tslt("Please upload a file (Only .csv Allowed)")</label>
					<input name="csv" type="file" class="form-control" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-exce">
				</div>
			</div>
			<div class="modal-footer">
				<button id="close-import" type="button" class="btn btn-default" data-dismiss="modal">@tslt("Close")</button>
				{!! Form::submit( 'Submit', ["id" => "import-modal", 'class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/warehouse_receipt_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "@tslt("Search")"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#warehouse_receipt-add-form").validate({
		
	});
	$("#wr-import-form").on("submit", function() {
		$("#import-modal").addClass("disabled");
		$("#close-import").hide();
		$("#import-modal").val("Be pacient, importing...");
	})
});
</script>
@endpush
