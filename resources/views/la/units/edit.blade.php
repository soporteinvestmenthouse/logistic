@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/units') }}">@tslt("Unit")</a> :
@endsection
@section("contentheader_description", __t($unit->$view_col))
@section("section", __t("Units"))
@section("section_url", url(config('laraadmin.adminRoute') . '/units'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("Units Edit : ".$unit->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($unit, ['route' => [config('laraadmin.adminRoute') . '.units.update', $unit->id ], 'method'=>'PUT', 'id' => 'unit-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'symbol')
					@la_input($module, 'transformation')
					@la_input($module, 'measures')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/units') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#unit-edit-form").validate({
		
	});
});
</script>
@endpush
