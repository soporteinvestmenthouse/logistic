@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/countries') }}">@tslt("Country")</a> :
@endsection
@section("contentheader_description", __t($country->$view_col))
@section("section", __t("Countries"))
@section("section_url", url(config('laraadmin.adminRoute') . '/countries'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("Countries Edit : ".$country->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($country, ['route' => [config('laraadmin.adminRoute') . '.countries.update', $country->id ], 'method'=>'PUT', 'id' => 'country-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'iso')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/countries') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#country-edit-form").validate({
		
	});
});
</script>
@endpush
