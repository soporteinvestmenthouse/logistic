@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/purchaseshipments') }}">@tslt("PurchaseShipment")</a> :
@endsection
@section("contentheader_description", __t($purchaseshipment->$view_col))
@section("section", __t("PurchaseShipments"))
@section("section_url", url(config('laraadmin.adminRoute') . '/purchaseshipments'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("PurchaseShipments Edit : ".$purchaseshipment->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($purchaseshipment, ['route' => [config('laraadmin.adminRoute') . '.purchaseshipments.update', $purchaseshipment->id ], 'method'=>'PUT', 'id' => 'purchaseshipment-edit-form']) !!}
					
					
					
					@la_form($module)
					
					{{--
					@la_input($module, 'shipment_id')
					@la_input($module, 'purchase_id')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/purchaseshipments') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#purchaseshipment-edit-form").validate({
		
	});
});
</script>
@endpush
