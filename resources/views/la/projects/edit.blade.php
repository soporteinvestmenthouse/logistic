@extends("la.layouts.app")

@section('contentheader_title')
    <a href="{{ url(config('laraadmin.adminRoute') . '/projects') }}">@tslt("Project")</a> :
@endsection
@section('contentheader_description', __t($project->$view_col))
@section('section', __t('Projects'))
@section('section_url', url(config('laraadmin.adminRoute') . '/projects'))
@section('sub_section', 'Edit')

@section('htmlheader_title', __t('Projects Edit : ' . $project->$view_col))

@section('main-content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                   @if($project->shipmentStatus)
                    <div class="badge bg-{{ $project->shipmentStatus->statusElement->color() }}">{{ $project->shipmentStatus->statusElement->name }}</div>

                   @endif
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::model($project, ['route' => [config('laraadmin.adminRoute') . '.projects.update', $project->id], 'method' => 'PUT', 'id' => 'project-edit-form']) !!}
                            
                            @la_input($module, 'code', null, null, "form-control", $project->shipmentStatus != null ? ["readonly" => "readonly"] : [])
							@la_input($module, 'travel_type_id', null, null, "form-control", $project->shipmentStatus != null ? ["readonly" => "readonly"] : [])
							@la_input($module, 'estimated_departure', null, null, "form-control", $project->shipmentStatus != null ? ["readonly" => "readonly"] : [])
							@la_input($module, 'store_id', null, null, "form-control", $project->shipmentStatus != null ? ["readonly" => "readonly"] : [])
							@la_input($module, 'receive_store_id', null, null, "form-control", $project->shipmentStatus != null ? ["readonly" => "readonly"] : [])
                            {{-- @la_form($module) --}}
                            <br>
                            @if($project->shipmentStatus == null)
                                <div class="form-group">
                                {!! Form::submit('Update', ['class' => 'btn btn-success']) !!} <button type="button" class="btn btn-warning pull-right" data-toggle="modal" data-target="#AddModal">@tslt("Close")</button>
                            </div>
                            {!! Form::close() !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            @include("la.warehouse_receipts.indexable", ["project" => $project])
        </div>

        <div class="col-md-8">
            <div class="box">
                <div class="box-header">
                    <h4 class="pull-left">Shipment status history</h4> <button
                        class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">@tslt("Add")
                        @tslt("Shipment Update")</button>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>@tslt("Status")</th>
                                        <th>@tslt("Date")</th>
                                        <th>@tslt("Message")</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($project->updates as $update)
                                        <tr>
                                            <td>
                                                <div class="btn btn-xs btn-{{ $update->statusElement->color() }}">
                                                    {{ $update->statusElement->name }}</div>
                                            </td>
                                            <td>{{ \Carbon\Carbon::parse($update->status_at)->format('d/m/Y H:i') }}</td>
                                            <td>{{ $update->message }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @php
    $shipmentModule = $module;
    $module = \DesarrollatuApp\NWCRM\Models\Module::get('Shipment_Updates');
    @endphp
    @la_access("Shipment_Updates", "create")
    <div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">@tslt("Add") @tslt("Shipment Update")</h4>
                </div>
                {!! Form::open(['action' => 'LA\Shipment_UpdatesController@store', 'id' => 'shipment_update-add-form']) !!}
                <div class="modal-body">
                    <div class="box-body" >
                        @if($project->shipmentStatus == null)
                            <div class="alert alert-warning" role="alert">
                                <p class="text-center"><strong><i class="fa fa-warning"></i> You're closing the project!</strong></p>

                                <p class="text-center">Project will be close and won't be avilable for edition</p>
                            </div>
                        @endif
                        {{-- @la_form($module) --}}

                        {{-- @la_input($module, 'shipment_id') --}}
                        <input type="hidden" value="{{ $project->id }}" name="project_id">
                        @la_input($module, 'status', $project->shipmentStatus == null ? 1 : null, null, 'form-control', $project->shipmentStatus == null ? ["readonly" => "readonly"] : [])
                        @if($project->shipmentStatus != null)
                        @la_input($module, 'message', null, null, 'form-control', $project->shipmentStatus == null ? ["readonly" => "readonly"] : [])
                        @endif
                        
                        @la_input($module, 'status_at', \Carbon\Carbon::now()->format("Y-m-d H:i:s"), null, 'form-control', $project->shipmentStatus == null ? ["readonly" => "readonly"] : [])
                        

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">@tslt("Close")</button>
                    {!! Form::submit('Submit', ['class' => 'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @php $module = $shipmentModule; @endphp
    @endla_access
@endsection

@push('scripts')
    <script>
        $(function() {
            $("#project-edit-form").validate({

            });
        });
    </script>
@endpush
