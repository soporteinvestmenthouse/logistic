@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/shipment_updates') }}">@tslt("Shipment Update")</a> :
@endsection
@section("contentheader_description", __t($shipment_update->$view_col))
@section("section", __t("Shipment Updates"))
@section("section_url", url(config('laraadmin.adminRoute') . '/shipment_updates'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("Shipment Updates Edit : ".$shipment_update->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($shipment_update, ['route' => [config('laraadmin.adminRoute') . '.shipment_updates.update', $shipment_update->id ], 'method'=>'PUT', 'id' => 'shipment_update-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'shipment_id')
					@la_input($module, 'message')
					@la_input($module, 'status_at')
					@la_input($module, 'status')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/shipment_updates') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#shipment_update-edit-form").validate({
		
	});
});
</script>
@endpush
