@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/claims') }}">@tslt("Claim")</a> :
@endsection
@section("contentheader_description", __t($claim->$view_col))
@section("section", __t("Claims"))
@section("section_url", url(config('laraadmin.adminRoute') . '/claims'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("Claims Edit : ".$claim->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($claim, ['route' => [config('laraadmin.adminRoute') . '.claims.update', $claim->id ], 'method'=>'PUT', 'id' => 'claim-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'tracking')
					@la_input($module, 'customer_name')
					@la_input($module, 'est_delivery_at')
					@la_input($module, 'claim_type_id')
					@la_input($module, 'message')
					@la_input($module, 'upload_id')
					@la_input($module, 'agent_id')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/claims') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#claim-edit-form").validate({
		
	});
});
</script>
@endpush
