@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/user_field_values') }}">@tslt("User Field Value")</a> :
@endsection
@section("contentheader_description", __t($user_field_value->$view_col))
@section("section", __t("User Field Values"))
@section("section_url", url(config('laraadmin.adminRoute') . '/user_field_values'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("User Field Values Edit : ".$user_field_value->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($user_field_value, ['route' => [config('laraadmin.adminRoute') . '.user_field_values.update', $user_field_value->id ], 'method'=>'PUT', 'id' => 'user_field_value-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'user_type_id')
					@la_input($module, 'client_id')
					@la_input($module, 'value')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/user_field_values') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#user_field_value-edit-form").validate({
		
	});
});
</script>
@endpush
