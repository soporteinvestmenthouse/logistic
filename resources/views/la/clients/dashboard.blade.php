@php
$user = Auth::user();
$client = $user->client;
@endphp
<section class="content">

    @if ($client->locker_number != null)
        <div class="box box-success">
            <div class="box-header">
                <i class="fa fa-box"></i>
                <h3 class="box-title">@tslt("Your shipments")</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        @if($client->hasShipments())
                            <h4>Your packages in miami</h4>
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="success">
                                        <th>WR No.</th>
                                        <th>Agency</th>
                                        <th>Contents</th>                                    
                                        <th>Weight</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($client->activeWr as $wr)
                                        <tr>
                                            <td>{{$wr->id}}</td>
                                            <td><img style="height:40px; border-radius:3px" src="{{\App\Models\Upload::find($wr->agency->logo_id)->path()}}" /> <strong>{{$wr->tracking_agency_code == null ? "N/A" : $wr->tracking_agency_code}}</strong></td>
                                            <td>{{$wr->package_content}}</td>
                                            <td></td>
                                            <td>
                                                @if(!$wr->readyToShip())
                                                   <a href="{{url(config('laraadmin.adminRoute') . '/warehouse_receipts/'.$wr->id.'/edit')}}" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;">Ingresar información</a>
                                                @else
                                                    <a href="{{url(config('laraadmin.adminRoute') . '/warehouse_receipts/'.$wr->id.'/edit')}}" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;">Editar</a>
                                                    <div class="badge bg-success"> Ready to Ship <i class="fa fa-check" style="color:green"></i></div>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            
                                <p>@tslt("You don't have any shipments waiting")</p>
                            
                        @endif
                    </div>
                </div>
            </div>
            <div class="box-footer clearfix">
                <button class="pull-right btn btn-default" id="sendEmail">@tslt("Register Purchase") <i
                        class="fa fa-arrow-circle-right"></i></button>
            </div>
        </div>



        <div class="box box-success">
            <div class="box-header">
                <i class="fa fa-box"></i>
                <h3 class="box-title">@tslt("Instructions")</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="gmail_quote">
                            <div dir="ltr">
                                <div class="gmail_quote"><strong style="text-transform:uppercase">{{$client->name}}</strong><br><br>

                                    Numero de casillero y datos para sus compras como siguen: <br><br>

                                    <strong>NOMBRE:</strong> {{$client->name}} <br><br>
                                    <strong>APELLIDO:</strong> <span style="text-transform:uppercase">{{$client->locker_number}}</span><br>
                                    (Al momento de realizar la compra acortar su nombre unicamente con su primer nombre
                                    y primer apellido y al final de su apellido debe colocar el numero de casillero para
                                    poder identificar las cargas al momento de la llegada a la bodeda de Miami. Si el
                                    sistema no le permite colocar numeros solo colocar el codigo ECPA)<br><br>

                                    <strong>DIRECCION MIAMI ENVIOS AEREOS:</strong><br>
                                    <strong>NOMBRE + APELLIDO + ECPA</strong><br>
                                    10049 NW 89TH AVE BAY 14,<br>
                                    Ciudad: MEDLEY (En caso tal que la pagina no acepte Medley entonces colocar
                                    MIAMI)<br>
                                    Estado: FLORIDA<br>
                                    Codigo Postal: 33178<br>
                                    Telefono: 7863914404<br><br>

                                    <strong>DIRECCION MIAMI ENVIOS MARITIMOS:</strong><br>
                                    <strong>NOMBRE + APELLIDO + ECPAM</strong><br>
                                    9999 NW 89TH AVE BAY 20,<br>
                                    Ciudad: MEDLEY (En caso tal que la pagina no acepte Medley entonces colocar
                                    MIAMI)<br>
                                    Estado: FLORIDA<br>
                                    Codigo Postal: 33178<br>
                                    Telefono: 3058878817<br><br>

                                    <strong>NOTA:</strong><br>
                                    CUANDO REALIZA COMPRAS POR SU CUENTA<br>
                                    FAVOR COLOCAR: NO ENTREGAR EN FIN DE SEMANA, YA QUE LA BODEGA NO TRABAJA FINES DE
                                    SEMANA.<br><br>

                                    <strong>Acceso a nuestro sistema para revision de llegadas de cargas y pre
                                        alertas:</strong><br>
                                    <a href="http://extremecargo2.managercargo.com/public/indexlogin/logincasillero"
                                        rel="noreferrer" target="_blank"
                                        data-saferedirecturl="https://www.google.com/url?q=http://extremecargo2.managercargo.com/public/indexlogin/logincasillero&amp;source=gmail&amp;ust=1635097840872000&amp;usg=AFQjCNEqODfbKq9kgf02uveiKkg_tX7nxQ">ENTRAR</a><br><br>

                                    USUARIO: <a href="mailto:{{$client->email}}" rel="noreferrer"
                                        target="_blank">{{$client->email}}</a><br>
                                    

                                    Se recomienda hacer el cambio de su clave dentro de su casillero, en la barra
                                    lateral izquierda donde dice actualizar mis datos, opcion password cambiar y
                                    guardar.<br><br>

                                    <strong>**IMPORTANTE:</strong><br>
                                    <strong>*</strong>Mercancias con valor comercial arriba de 100.00 USD debera pagar
                                    impuestos segun el producto.<br>
                                    <strong>*</strong>Toda mercancia como maquillajes, pastillas, cremas o untables en
                                    el cuerpo no son permitidos en exceso y debe llenarse el formulario de farmacia y
                                    droga para su debido proceso.<br>
                                    <strong>*</strong>Las facturas de compras deben ser enviada por correo (<a
                                        href="mailto:info@extreme-cargo.com" rel="noreferrer"
                                        target="_blank">info@extreme-cargo.com</a>) para el proceso de aduana de la
                                    misma.<br><br>

                                    <strong>SIGUENOS EN INSTAGRAM: @<a href="http://extremecargo.pa" rel="noreferrer"
                                            target="_blank"
                                            data-saferedirecturl="https://www.google.com/url?q=http://extremecargo.pa&amp;source=gmail&amp;ust=1635097840872000&amp;usg=AFQjCNHw6qeeNMuS83-vu6tDgzzeTJUJKQ">extremecargo.pa</a></strong><br><br>

                                    <strong>Este correo es solo para la creacion del casillero. No escribir a este
                                        email. Cualquier consulta escribir a <a href="mailto:info@extreme-cargo.com"
                                            rel="noreferrer"
                                            target="_blank">info@extreme-cargo.com</a></strong><br><br><br>
                                    <hr>



                                    <strong>SIGUENOS EN INSTAGRAM: @<a href="http://extremecargo.pa" rel="noreferrer"
                                            target="_blank"
                                            data-saferedirecturl="https://www.google.com/url?q=http://extremecargo.pa&amp;source=gmail&amp;ust=1635097840872000&amp;usg=AFQjCNHw6qeeNMuS83-vu6tDgzzeTJUJKQ">extremecargo.pa</a></strong><br><br>




                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer clearfix">
                <button class="pull-right btn btn-default" id="sendEmail">@tslt("Register Purchase") <i
                        class="fa fa-arrow-circle-right"></i></button>
            </div>
        </div>
    @else
        @php
            $clientModule = \DesarrollatuApp\NWCRM\Models\Module::get('Clients');
        @endphp
        <div class="box box-warning">
            <div class="box-header">
                <i class="fa fa-info"></i>
                <h3 class="box-title">@tslt("Finish your information")</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        {!! Form::model($client, ['route' => [config('laraadmin.adminRoute') . '.clients.update', $client->id], 'method' => 'PUT', 'id' => 'client-edit-form']) !!}
                        {{-- @la_form($clientModule) --}}

                        @la_input($clientModule, 'user_type_id')
                        @la_input($clientModule, 'name')
                        @la_input($clientModule, 'email')
                        @la_input($clientModule, 'user_id')
                        @la_input($clientModule, 'branch_id')
                        @la_input($clientModule, 'locker_number')
                        @la_input($clientModule, 'address')
                        @la_input($clientModule, 'phone')
                        @la_input($clientModule, 'birthdate')
                        <br>
                        <div class="form-group">
                            {!! Form::submit('Update', ['class' => 'btn btn-success']) !!} <button class="btn btn-default pull-right"><a
                                    href="{{ url(config('laraadmin.adminRoute') . '/clients') }}">@tslt("Cancel")</a></button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="box-footer clearfix">

            </div>
        </div>
    @endif
</section>
