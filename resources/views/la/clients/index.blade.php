@extends("la.layouts.app")

@section("contentheader_title", __t("Clients"))
@section("contentheader_description", __t("Clients listing"))
@section("section", __t("Clients"))
@section("sub_section", __t("Listing"))
@section("htmlheader_title", __t("Clients Listing"))

@section("headerElems")
@la_access("Clients", "create")
	<button class="btn btn-primary btn-sm pull-right"  style="margin-left:10px" data-toggle="modal" data-target="#ImportModal">@tslt("Import") @tslt("Clients")</button>	
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">@tslt("Add") @tslt("Client")</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ isset($module->fields[$col]['label']) ? __t($module->fields[$col]['label']) : __t(ucfirst($col)) }}</th>
			@endforeach
			@if($show_actions)
			<th>@tslt("Actions")</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Clients", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">@tslt("Add") @tslt("Client")</h4>
			</div>
			{!! Form::open(['action' => 'LA\ClientsController@store', 'id' => 'client-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    
					@la_input($module, 'user_type_id')
					@la_input($module, 'name')
					@la_input($module, 'email')
					@la_input($module, 'branch_id')
					@la_input($module, 'address')
					@la_input($module, 'phone')
					@la_input($module, 'birthdate')
					{{--
					@la_form($module)
					@la_input($module, 'user_id')					
					@la_input($module, 'locker_number')
					--}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">@tslt("Close")</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<div class="modal fade" id="ImportModal" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">@tslt("Add") @tslt("Client")</h4>
			</div>
			{!! Form::open(['action' => 'LA\ClientsController@import', 'id' => 'client-import-form', "enctype" => "multipart/form-data"]) !!}
			<div class="modal-body">
				<div class="box-body">
                   	<div class="form-group">
					   	<label for="csv" class="label-control">@tslt("Please upload a file (Only .csv Allowed)")</label>
						<input name="csv" type="file" class="form-control" accept=".csv">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="close-import" type="button" class="btn btn-default" data-dismiss="modal">@tslt("Close")</button>
				{!! Form::submit( 'Submit', ["id" => "import-modal", 'class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/client_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "@tslt("Search")"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#client-add-form").validate({
		
	});

	$("#client-import-form").on("submit", function() {
		$("#import-modal").addClass("disabled");
		$("#close-import").hide();
		$("#import-modal").val("Be pacient, importing...");
	})
});
</script>
@endpush
