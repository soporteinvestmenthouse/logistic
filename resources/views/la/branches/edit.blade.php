@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/branches') }}">@tslt("Branch")</a> :
@endsection
@section("contentheader_description", __t($branch->$view_col))
@section("section", __t("Branches"))
@section("section_url", url(config('laraadmin.adminRoute') . '/branches'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("Branches Edit : ".$branch->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($branch, ['route' => [config('laraadmin.adminRoute') . '.branches.update', $branch->id ], 'method'=>'PUT', 'id' => 'branch-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'email')
					@la_input($module, 'address')
					@la_input($module, 'prefix')
					@la_input($module, 'upload_id')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/branches') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#branch-edit-form").validate({
		
	});
});
</script>
@endpush
