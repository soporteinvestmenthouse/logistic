@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/user_types') }}">@tslt("User Type")</a> :
@endsection
@section("contentheader_description", __t($user_type->$view_col))
@section("section", __t("User Types"))
@section("section_url", url(config('laraadmin.adminRoute') . '/user_types'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("User Types Edit : ".$user_type->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($user_type, ['route' => [config('laraadmin.adminRoute') . '.user_types.update', $user_type->id ], 'method'=>'PUT', 'id' => 'user_type-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'custom_fields')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/user_types') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#user_type-edit-form").validate({
		
	});
});
</script>
@endpush
