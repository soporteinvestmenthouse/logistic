@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/wrimports') }}">@tslt("WRImport")</a> :
@endsection
@section("contentheader_description", __t($wrimport->$view_col))
@section("section", __t("WRImports"))
@section("section_url", url(config('laraadmin.adminRoute') . '/wrimports'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("WRImports Edit : ".$wrimport->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($wrimport, ['route' => [config('laraadmin.adminRoute') . '.wrimports.update', $wrimport->id ], 'method'=>'PUT', 'id' => 'wrimport-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'imported_by')
					@la_input($module, 'success')
					@la_input($module, 'file_id')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/wrimports') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#wrimport-edit-form").validate({
		
	});
});
</script>
@endpush
