@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/follow_ups') }}">@tslt("Follow Up")</a> :
@endsection
@section("contentheader_description", __t($follow_up->$view_col))
@section("section", __t("Follow Ups"))
@section("section_url", url(config('laraadmin.adminRoute') . '/follow_ups'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("Follow Ups Edit : ".$follow_up->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($follow_up, ['route' => [config('laraadmin.adminRoute') . '.follow_ups.update', $follow_up->id ], 'method'=>'PUT', 'id' => 'follow_up-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'purchase_id')
					@la_input($module, 'owner_id')
					@la_input($module, 'type')
					@la_input($module, 'severity')
					@la_input($module, 'responds_to')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/follow_ups') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#follow_up-edit-form").validate({
		
	});
});
</script>
@endpush
