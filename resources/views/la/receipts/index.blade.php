@extends("la.layouts.app")

@section("contentheader_title", __t("Receipts"))
@section("contentheader_description", __t("Receipts listing"))
@section("section", __t("Receipts"))
@section("sub_section", __t("Listing"))
@section("htmlheader_title", __t("Receipts Listing"))

@section("headerElems")
@la_access("Receipts", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">@tslt("Add") @tslt("Receipt")</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ isset($module->fields[$col]['label']) ? __t($module->fields[$col]['label']) : __t(ucfirst($col)) }}</th>
			@endforeach
			@if($show_actions)
			<th>@tslt("Actions")</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Receipts", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">@tslt("Add") @tslt("Receipt")</h4>
			</div>
			{!! Form::open(['action' => 'LA\ReceiptsController@store', 'id' => 'receipt-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    @la_form($module)
					
					{{--
					@la_input($module, 'shipment')
					@la_input($module, 'responsible_id')
					@la_input($module, 'client_id')
					@la_input($module, 'received_by_client')
					@la_input($module, 'receiver_name')
					@la_input($module, 'receiver_doc')
					@la_input($module, 'package_img_id')
					@la_input($module, 'signature_id')
					--}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">@tslt("Close")</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/receipt_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "@tslt("Search")"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#receipt-add-form").validate({
		
	});
});
</script>
@endpush
