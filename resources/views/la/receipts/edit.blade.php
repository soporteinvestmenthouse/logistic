@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/receipts') }}">@tslt("Receipt")</a> :
@endsection
@section("contentheader_description", __t($receipt->$view_col))
@section("section", __t("Receipts"))
@section("section_url", url(config('laraadmin.adminRoute') . '/receipts'))
@section("sub_section", "Edit")

@section("htmlheader_title", __t("Receipts Edit : ".$receipt->$view_col))

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($receipt, ['route' => [config('laraadmin.adminRoute') . '.receipts.update', $receipt->id ], 'method'=>'PUT', 'id' => 'receipt-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'shipment')
					@la_input($module, 'responsible_id')
					@la_input($module, 'client_id')
					@la_input($module, 'received_by_client')
					@la_input($module, 'receiver_name')
					@la_input($module, 'receiver_doc')
					@la_input($module, 'package_img_id')
					@la_input($module, 'signature_id')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/receipts') }}">@tslt("Cancel")</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#receipt-edit-form").validate({
		
	});
});
</script>
@endpush
