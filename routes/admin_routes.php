<?php

/* ================== Homepage ================== */
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::auth();

/* ================== Access Uploaded Files ================== */
Route::get('files/{hash}/{name}', 'LA\UploadsController@get_file');

/*
|--------------------------------------------------------------------------
| Admin Application Routes
|--------------------------------------------------------------------------
*/

$as = "";
if(\DesarrollatuApp\NWCRM\Helpers\LAHelper::laravel_ver() >= 5.3) {
	$as = config('laraadmin.adminRoute').'.';
	
	// Routes for Laravel 5.3
	Route::get('/logout', 'Auth\LoginController@logout');
}

Route::group(['as' => $as, 'middleware' => ['auth', 'permission:ADMIN_PANEL']], function () {
	
	/* ================== Dashboard ================== */
	
	Route::get(config('laraadmin.adminRoute'), 'LA\DashboardController@index');
	Route::get(config('laraadmin.adminRoute'). '/dashboard', 'LA\DashboardController@index');
	
	/* ================== Users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/users', 'LA\UsersController');
	Route::get(config('laraadmin.adminRoute') . '/user_dt_ajax', 'LA\UsersController@dtajax');
	
	/* ================== Uploads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/uploads', 'LA\UploadsController');
	Route::post(config('laraadmin.adminRoute') . '/upload_files', 'LA\UploadsController@upload_files');
	Route::get(config('laraadmin.adminRoute') . '/uploaded_files', 'LA\UploadsController@uploaded_files');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_caption', 'LA\UploadsController@update_caption');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_filename', 'LA\UploadsController@update_filename');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_public', 'LA\UploadsController@update_public');
	Route::post(config('laraadmin.adminRoute') . '/uploads_delete_file', 'LA\UploadsController@delete_file');
	
	/* ================== Roles ================== */
	Route::resource(config('laraadmin.adminRoute') . '/roles', 'LA\RolesController');
	Route::get(config('laraadmin.adminRoute') . '/role_dt_ajax', 'LA\RolesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_module_role_permissions/{id}', 'LA\RolesController@save_module_role_permissions');
	
	/* ================== Permissions ================== */
	Route::resource(config('laraadmin.adminRoute') . '/permissions', 'LA\PermissionsController');
	Route::get(config('laraadmin.adminRoute') . '/permission_dt_ajax', 'LA\PermissionsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_permissions/{id}', 'LA\PermissionsController@save_permissions');
	
	/* ================== Departments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/departments', 'LA\DepartmentsController');
	Route::get(config('laraadmin.adminRoute') . '/department_dt_ajax', 'LA\DepartmentsController@dtajax');
	
	/* ================== Employees ================== */
	Route::resource(config('laraadmin.adminRoute') . '/employees', 'LA\EmployeesController');
	Route::get(config('laraadmin.adminRoute') . '/employee_dt_ajax', 'LA\EmployeesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/change_password/{id}', 'LA\EmployeesController@change_password');
	
	/* ================== Organizations ================== */
	Route::resource(config('laraadmin.adminRoute') . '/organizations', 'LA\OrganizationsController');
	Route::get(config('laraadmin.adminRoute') . '/organization_dt_ajax', 'LA\OrganizationsController@dtajax');

	/* ================== Backups ================== */
	Route::resource(config('laraadmin.adminRoute') . '/backups', 'LA\BackupsController');
	Route::get(config('laraadmin.adminRoute') . '/backup_dt_ajax', 'LA\BackupsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/create_backup_ajax', 'LA\BackupsController@create_backup_ajax');
	Route::get(config('laraadmin.adminRoute') . '/downloadBackup/{id}', 'LA\BackupsController@downloadBackup');

	/* ================== Languages ================== */
	Route::resource(config('laraadmin.adminRoute') . '/languages', 'LA\LanguagesController');
	Route::get(config('laraadmin.adminRoute') . '/language_dt_ajax', 'LA\LanguagesController@dtajax');
	Route::get(config('laraadmin.adminRoute') . '/languages/google_translate/{id}', 'LA\LanguagesController@google_translate');

	/* ================== Branches ================== */
	Route::resource(config('laraadmin.adminRoute') . '/branches', 'LA\BranchesController');
	Route::get(config('laraadmin.adminRoute') . '/branch_dt_ajax', 'LA\BranchesController@dtajax');

	/* ================== Clients ================== */
	Route::resource(config('laraadmin.adminRoute') . '/clients', 'LA\ClientsController');
	Route::post(config('laraadmin.adminRoute') . '/clients/import', 'LA\ClientsController@import');
	Route::get(config('laraadmin.adminRoute') . '/client_dt_ajax', 'LA\ClientsController@dtajax');

	/* ================== User_Types ================== */
	Route::resource(config('laraadmin.adminRoute') . '/user_types', 'LA\User_TypesController');
	Route::get(config('laraadmin.adminRoute') . '/user_type_dt_ajax', 'LA\User_TypesController@dtajax');

	/* ================== User_Fields ================== */
	Route::resource(config('laraadmin.adminRoute') . '/user_fields', 'LA\User_FieldsController');
	Route::get(config('laraadmin.adminRoute') . '/user_field_dt_ajax', 'LA\User_FieldsController@dtajax');

	/* ================== User_Field_Values ================== */
	Route::resource(config('laraadmin.adminRoute') . '/user_field_values', 'LA\User_Field_ValuesController');
	Route::get(config('laraadmin.adminRoute') . '/user_field_value_dt_ajax', 'LA\User_Field_ValuesController@dtajax');

	/* ================== Shipment_Agencies ================== */
	Route::resource(config('laraadmin.adminRoute') . '/shipment_agencies', 'LA\Shipment_AgenciesController');
	Route::get(config('laraadmin.adminRoute') . '/shipment_agency_dt_ajax', 'LA\Shipment_AgenciesController@dtajax');

	/* ================== Purchases ================== */
	Route::resource(config('laraadmin.adminRoute') . '/purchases', 'LA\PurchasesController');
	Route::get(config('laraadmin.adminRoute') . '/purchase_dt_ajax', 'LA\PurchasesController@dtajax');

	/* ================== Follow_Ups ================== */
	Route::resource(config('laraadmin.adminRoute') . '/follow_ups', 'LA\Follow_UpsController');
	Route::get(config('laraadmin.adminRoute') . '/follow_up_dt_ajax', 'LA\Follow_UpsController@dtajax');

	/* ================== Countries ================== */
	Route::resource(config('laraadmin.adminRoute') . '/countries', 'LA\CountriesController');
	Route::get(config('laraadmin.adminRoute') . '/country_dt_ajax', 'LA\CountriesController@dtajax');

	/* ================== Stores ================== */
	Route::resource(config('laraadmin.adminRoute') . '/stores', 'LA\StoresController');
	Route::get(config('laraadmin.adminRoute') . '/store_dt_ajax', 'LA\StoresController@dtajax');

	/* ================== Shipment_Travel_Types ================== */
	Route::resource(config('laraadmin.adminRoute') . '/shipment_travel_types', 'LA\Shipment_Travel_TypesController');
	Route::get(config('laraadmin.adminRoute') . '/shipment_travel_type_dt_ajax', 'LA\Shipment_Travel_TypesController@dtajax');

	/* ================== Shipments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/shipments', 'LA\ShipmentsController');
	Route::get(config('laraadmin.adminRoute') . '/shipment_dt_ajax', 'LA\ShipmentsController@dtajax');

	/* ================== Receipts ================== */
	Route::resource(config('laraadmin.adminRoute') . '/receipts', 'LA\ReceiptsController');
	Route::get(config('laraadmin.adminRoute') . '/receipt_dt_ajax', 'LA\ReceiptsController@dtajax');

	/* ================== Failed_Deliveries ================== */
	Route::resource(config('laraadmin.adminRoute') . '/failed_deliveries', 'LA\Failed_DeliveriesController');
	Route::get(config('laraadmin.adminRoute') . '/failed_delivery_dt_ajax', 'LA\Failed_DeliveriesController@dtajax');

	/* ================== Units ================== */
	Route::resource(config('laraadmin.adminRoute') . '/units', 'LA\UnitsController');
	Route::get(config('laraadmin.adminRoute') . '/unit_dt_ajax', 'LA\UnitsController@dtajax');

	/* ================== Shipment_Statuses ================== */
	Route::resource(config('laraadmin.adminRoute') . '/shipment_statuses', 'LA\Shipment_StatusesController');
	Route::get(config('laraadmin.adminRoute') . '/shipment_status_dt_ajax', 'LA\Shipment_StatusesController@dtajax');

	/* ================== Shipment_Updates ================== */
	Route::resource(config('laraadmin.adminRoute') . '/shipment_updates', 'LA\Shipment_UpdatesController');
	Route::get(config('laraadmin.adminRoute') . '/shipment_update_dt_ajax', 'LA\Shipment_UpdatesController@dtajax');

	/* ================== PurchaseShipments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/purchaseshipments', 'LA\PurchaseShipmentsController');
	Route::get(config('laraadmin.adminRoute') . '/purchaseshipment_dt_ajax', 'LA\PurchaseShipmentsController@dtajax');

	/* ================== Package_Types ================== */
	Route::resource(config('laraadmin.adminRoute') . '/package_types', 'LA\Package_TypesController');
	Route::get(config('laraadmin.adminRoute') . '/package_type_dt_ajax', 'LA\Package_TypesController@dtajax');

	/* ================== Packages ================== */
	Route::resource(config('laraadmin.adminRoute') . '/packages', 'LA\PackagesController');
	Route::get(config('laraadmin.adminRoute') . '/package_dt_ajax/{whr_id?}', 'LA\PackagesController@dtajax');

	/* ================== Warehouse_Receipts ================== */
	Route::resource(config('laraadmin.adminRoute') . '/warehouse_receipts', 'LA\Warehouse_ReceiptsController');
	Route::get(config('laraadmin.adminRoute') . '/warehouse_receipt_dt_ajax/{project_id?}', 'LA\Warehouse_ReceiptsController@dtajax');

	/* ================== WRImports ================== */
	Route::resource(config('laraadmin.adminRoute') . '/wrimports', 'LA\WRImportsController');
	Route::get(config('laraadmin.adminRoute') . '/wrimport_dt_ajax', 'LA\WRImportsController@dtajax');

	/* ================== Purchase_Articles ================== */
	Route::resource(config('laraadmin.adminRoute') . '/purchase_articles', 'LA\Purchase_ArticlesController');
	Route::get(config('laraadmin.adminRoute') . '/purchase_article_dt_ajax', 'LA\Purchase_ArticlesController@dtajax');

	/* ================== Claim_Types ================== */
	Route::resource(config('laraadmin.adminRoute') . '/claim_types', 'LA\Claim_TypesController');
	Route::get(config('laraadmin.adminRoute') . '/claim_type_dt_ajax', 'LA\Claim_TypesController@dtajax');

	/* ================== Claims ================== */
	Route::resource(config('laraadmin.adminRoute') . '/claims', 'LA\ClaimsController');
	Route::get(config('laraadmin.adminRoute') . '/claim_dt_ajax', 'LA\ClaimsController@dtajax');

	/* ================== ClientRegisters ================== */
	Route::resource(config('laraadmin.adminRoute') . '/clientregisters', 'LA\ClientRegistersController');
	Route::get(config('laraadmin.adminRoute') . '/clientregister_dt_ajax', 'LA\ClientRegistersController@dtajax');

	/* ================== Projects ================== */
	Route::resource(config('laraadmin.adminRoute') . '/projects', 'LA\ProjectsController');
	Route::get(config('laraadmin.adminRoute') . '/project_dt_ajax', 'LA\ProjectsController@dtajax');
});
