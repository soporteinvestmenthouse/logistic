<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get("v1/models/{model}/manage/{id}", "Api\\ModelController@edit")->middleware("auth:api");
Route::get("v1/models/{model}/popupVals", "Api\\ModelController@popupVals")->middleware("auth:api");
Route::get("v1/models/{model}", "Api\\ModelController@index")->middleware("auth:api");
Route::get("v1/models/{model}/instance/{id}", "Api\\ModelController@show")->middleware("auth:api");
Route::post("v1/models/{model}", "Api\\ModelController@store")->middleware("auth:api");
Route::post("v1/models/{model}/import", "Api\\ModelController@import")->middleware("auth:api");
Route::get("v1/models/{model}/delete/{id}", "Api\\ModelController@destroy")->middleware("auth:api");
Route::get("v1/file/{id}", "LA\\UploadsController@getById")->middleware("auth:api");
Route::get("v1/models/popupQuery/{model}", "Api\\ModelController@popupQuery")->middleware("auth:api");
Route::post("v1/popupVals", "Api\\ModelController@popupVals")->middleware("auth:api");
Route::post("v1/models/{model}/update/{id}", "Api\\ModelController@update")->middleware("auth:api");
Route::post("v1/models/{model}/multidelete", "Api\\ModelController@multiDelete")->middleware("auth:api");
